﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces.Email;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace Infrastructure.Emails
{
    public partial class Email //: IGmail
    {
        private void Gmail(ConfigEmail config, List<string> to, string subject, string body)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("", config.Username));
            message.To.AddRange(to.Select(s => new MailboxAddress("", s)));
            message.Subject = subject;
            message.Body = new TextPart(TextFormat.Html) { Text = body };

            using var client = new SmtpClient();
            client.Connect(config.Host, config.Port, SecureSocketOptions.StartTls);
            client.Authenticate(config.Username, config.Password);
            client.Send(message);
            client.Disconnect(true);
        }


    }
}
