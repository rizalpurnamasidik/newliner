﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces.Email;
using Microsoft.Extensions.Options;
using Shared.Constants;
using Shared.Extensions;

namespace Infrastructure.Emails
{
    public partial class Email : IEmail
    {
        private readonly InfrastructureSettings _config;

        public Email(
            IOptions<InfrastructureSettings> config
            )
        {
            _config = config.Value;
        }


        public void Send(string emailContext, List<string> to, string subject, string body)
        {
            // get config
            var config = _config.Email
                .Where(w => w.EmailContext.ToLower() == emailContext.ToLower()).FirstOrDefault();

            if (config.Provider == ProviderEmail_Type.Gmail)
            {
                Gmail(config, to, subject, body);
            }

        }

    }
}
