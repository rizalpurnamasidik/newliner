﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Infrastructure;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using Shared.Extensions;

namespace Infrastructure.MessageBrokers
{

    // https://dotnetdocs.ir/Post/43/implementing-rabbitmq-in-net-core
    public class RabbitMq //: IRabbitMq
    {
        protected string _name;
        private readonly InfrastructureSettings _options;

        public RabbitMq(IOptions<InfrastructureSettings> options)
        {
            _options = options.Value;
        }

        public IConnection CreateChannel()
        {
            var caller = Class_Extension.NameOfCallingClass();
            var config = _options.MessageBroker.Where(w => w.Name.ToLower() == caller.ToLower()).FirstOrDefault();

            var connection = new ConnectionFactory
            {
                UserName = config.Username,
                Password = config.Password,
                HostName = config.Host,
                Port = config.Port,
                DispatchConsumersAsync = true
            };
            var channel = connection.CreateConnection();
            return channel;
        }
    }
}
