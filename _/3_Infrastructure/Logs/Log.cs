﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces.HttpContext;
using Application.Interfaces.Logs;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Logs
{
    public class Log : ILog
    {
        private readonly ILogger<Log> _logger;
        private readonly IHttpContext _httpContext;

        public Log(
            ILogger<Log> logger
            , IHttpContext httpContext
            )
        {
            _logger = logger;
            _httpContext = httpContext;
        }

        public void LogInformation(string? message, params object?[] args)
        {
            _logger.LogInformation($"[{_httpContext.CorrelationId}] {message}", args);
        }

        public void LogError(string? message, params object?[] args)
        {
            _logger.LogError($"[{_httpContext.CorrelationId}] {message}", args);
        }

        public string ErrorId()
        {
            return _httpContext.CorrelationId;
        }
    }
}
