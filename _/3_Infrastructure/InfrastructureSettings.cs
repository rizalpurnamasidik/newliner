﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Constants;

namespace Infrastructure
{
    public class InfrastructureSettings
    {
        public List<Database> Database { get; set; } = new();
        public List<Cryptography> Cryptography { get; set; } = new();
        public List<MessageBroker> MessageBroker { get; set; } = new();
        public Jwt Jwt { get; set; } = new();

        public List<ConfigEmail> Email { get; set; } = new();

        public void Merge(InfrastructureSettings settings)
        {
            Database.AddRange(settings.Database);
            Cryptography.AddRange(settings.Cryptography);
            MessageBroker.AddRange(settings.MessageBroker);
            Email.AddRange(settings.Email);

            if (!string.IsNullOrEmpty(settings.Jwt.Secret))
            {
                Jwt = settings.Jwt;
            }
        }
    }

    public class Database
    {
        public string DbContext { get; set; }
        public ProviderDatabase_Type Provider { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string DatabaseName { get; set; }
        public string DatabaseUsername { get; set; }
        public string DatabasePassword { get; set; }
    }

    public class Cryptography
    {
        public ProviderCryptography_Type Provider { get; set; }
        public string Key { get; set; }
    }

    public class MessageBroker
    {
        public string Name { get; set; }
        public ProviderMessageBroker_Type Provider { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class Jwt
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public int ExpireInSeconds { get; set; }
        public int ExpireInMinutes { get; set; }
        public int ExpireInHours { get; set; }
    }

    public class ConfigEmail
    {
        public string EmailContext { get; set; }
        public ProviderEmail_Type Provider { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
