﻿using Application.Interfaces;
using Application.Interfaces.Cryptography;
using Application.Interfaces.Email;
using Application.Interfaces.HttpContext;
using Application.Interfaces.Logs;
using Infrastructure.Cryptographys;
using Infrastructure.Emails;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Shared.Extensions;
using System.Reflection;

namespace Infrastructure
{
    public static class Startup
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Serilog();
            services.RegisterTools();
            return services;
        }

        private static IServiceCollection RegisterTools(this IServiceCollection services)
        {
            services.AddScoped<ICryptography, Cryptographys.Cryptography>();
            services.AddScoped<IUserLogin, UserLogin>();

            services.AddScoped<IHttpContext, HttpContext.HttpContext>();
            services.AddScoped<IEmail, Email>();
            services.AddScoped<ILog, Logs.Log>();
            return services;

        }

        private static IServiceCollection Serilog(this IServiceCollection services)
        {
            var template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}";
            Log.Logger = new LoggerConfiguration()
              //.ReadFrom.Configuration(builder.Build())
              .Enrich.FromLogContext()
              //.Enrich.WithProcessId()
              //.Enrich.WithThreadId()
              //.Enrich.WithThreadName()
              .WriteTo.Console(outputTemplate: template)

          //.WriteTo.Console()
          .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
          //.WriteTo.File(new JsonFormatter(), "logs/log.txt", Serilog.Events.LogEventLevel.Information, rollingInterval: RollingInterval.Hour)
          //.WriteTo.File("logs/information-.txt", LogEventLevel.Information, outputTemplate: template, rollingInterval: RollingInterval.Hour)
          .WriteTo.File("logs/debug-.txt", LogEventLevel.Debug, outputTemplate: template, rollingInterval: RollingInterval.Hour)
          .WriteTo.File("logs/error-.txt", LogEventLevel.Warning, outputTemplate: template, rollingInterval: RollingInterval.Hour)
          .CreateLogger();

            return services;

        }

        public static IServiceCollection LoadSettings<TSetting>(this IServiceCollection services, ConfigurationManager configuration)
            where TSetting : InfrastructureSettings, new()
        {
            var assemblyName = Assembly.GetCallingAssembly().GetName().Name;
            var allSettings = new TSetting();
            var allSettingsFilename = @$"{assemblyName}-cache-{Guid.NewGuid()}.json";

            // delete all cache setting
            var deletedSettingFIles = Directory.GetFiles($@"{AppContext.BaseDirectory}", @$"{assemblyName}-cache-*.json");
            foreach (var file in deletedSettingFIles)
            {
                File.Delete(file);
            }

            // get all setting
            var baseDirectory = AppContext.BaseDirectory;
            var files = Directory.GetFiles(Path.Combine(new string[] { baseDirectory, "Settings" }), "*.json");
            foreach (var file in files)
            {
                var jsonString = File.ReadAllText(file);
                allSettings.Merge(jsonString.ToJsonObject<TSetting>(removeAllComment: true));
            }

            // create a file
            File.WriteAllText(Path.Combine(new string[] { baseDirectory, allSettingsFilename }), allSettings.ToJsonString());

            // load setting 
            configuration.AddJsonFile(Path.Combine(new string[] { baseDirectory, allSettingsFilename }));


            services.Configure<InfrastructureSettings>(configuration);
            services.Configure<TSetting>(configuration);
            return services;

        }


        public static IServiceCollection AddDbContext(this IServiceCollection services, ConfigurationManager configuration, Action<IServiceCollection> dbContext)
        {
            dbContext(services);
            return services;
        }

        public static IServiceCollection AddMessageBroker(this IServiceCollection services, ConfigurationManager configuration, Action<IServiceCollection> messageBroker)
        {
            messageBroker(services);
            return services;
        }

    }
}
