﻿using System.Text;
using Application.Interfaces.HttpContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;

namespace Infrastructure.HttpContext
{
    public class HttpContext : IHttpContext
    {
        public string BaseUrl { get; set; }
        public string CorrelationId { get; set; }

        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            BaseUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}{_httpContextAccessor.HttpContext.Request.PathBase}";


            SetCorrelationId();
        }

        private void SetCorrelationId()
        {
            _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("Cko-Correlation-Id", out var correlationId);
            CorrelationId = correlationId.FirstOrDefault() ?? _httpContextAccessor.HttpContext.TraceIdentifier;
            CorrelationId = WebEncoders.Base64UrlEncode(Encoding.ASCII.GetBytes(CorrelationId));
        }


    }
}
