﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using _0_Shared.Constants;
using _0_Shared.Extensions;
using _2_Application.Classes;
using _2_Application.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Infrastructure
{
    public class UserSession : IUserSession
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICryptography _cryptography;

        public UserSession(IHttpContextAccessor httpContextAccessor, ICryptography cryptography)
        {
            _httpContextAccessor = httpContextAccessor;
            _cryptography = cryptography;
        }

        public bool IsAuthenticated => _httpContextAccessor.HttpContext is not null && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;

        public string Email
        {
            get
            {
                var cliamEmail = ClaimType.Email.ToDescription();

                if (_httpContextAccessor.HttpContext is null)
                { return "[SystemBackgroundJob]"; }
                if (_httpContextAccessor.HttpContext?.User?.FindFirstValue(cliamEmail) is null)
                { return "[Unknown_User]"; }
                var data = _httpContextAccessor.HttpContext?.User?.FindFirstValue(cliamEmail);
                return _cryptography.AES_Decrypt(data) ?? "[Unknown_User]";
            }

            set
            {

                if (value != null)
                {
                    //_httpContextAccessor.HttpContext.Response.Cookies.Append(
                    //    "E"
                    //    , _cryptography.AES_Encrypt(value)
                    //    , new CookieOptions
                    //    {
                    //        HttpOnly = true,
                    //        Expires = DateTime.UtcNow.AddDays(7),

                    //    });
                }
            }

        }

        public List<MenuPrivilege> MenuPrivilege
        {
            get
            {
                var cliam = ClaimType.MenuPrivilege.ToDescription();

                if (_httpContextAccessor.HttpContext is null)
                { return new List<MenuPrivilege>(); }
                if (_httpContextAccessor.HttpContext?.User?.FindFirstValue(cliam) is null)
                { return new List<MenuPrivilege>(); }
                var data = _httpContextAccessor.HttpContext?.User?.FindFirstValue(cliam);
                if (data == "")
                { return new List<MenuPrivilege>(); }
                return _cryptography.AES_Decrypt(data).ToJsonObject<List<MenuPrivilege>>() ?? new List<MenuPrivilege>();
            }

            set
            {

                if (value != null)
                {
                    //_httpContextAccessor.HttpContext.Response.Cookies.Append(
                    //    "E"
                    //    , _cryptography.AES_Encrypt(value)
                    //    , new CookieOptions
                    //    {
                    //        HttpOnly = true,
                    //        Expires = DateTime.UtcNow.AddDays(7),

                    //    });
                }
            }

        }
    }
}
