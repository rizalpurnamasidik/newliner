﻿using System.Data;
using System.Runtime.CompilerServices;
using Application.Interfaces;
using Domain.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Npgsql;
using Shared.Constants;
using Shared.Exceptions;
using Shared.Extensions;

namespace Infrastructure.DataSources
{
    public class DatabaseContext : DbContext
    {
        protected string _dapperConstring = "";
        protected ProviderDatabase_Type _provider;
        private readonly InfrastructureSettings _config;
        private readonly IUserLogin _userLogin;

        public DatabaseContext(DbContextOptions options,
            IOptions<InfrastructureSettings> config
            , IUserLogin userLogin
            ) : base(options)
        {
            _config = config.Value;
            _userLogin = userLogin;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            Save();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            Save();
            return base.SaveChanges();
        }

        private void Save()
        {
            foreach (var entry in ChangeTracker.Entries<IAuditable_Entity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = DateTimeOffset.UtcNow;
                        entry.Entity.CreatedBy = _userLogin.Email;
                        break;
                    case EntityState.Modified:
                        entry.Entity.ModifiedDate = DateTimeOffset.UtcNow;
                        entry.Entity.ModifiedBy = _userLogin.Email;

                        if (string.IsNullOrEmpty(entry.Entity.CreatedBy))
                        {
                            entry.Entity.CreatedBy = _userLogin.Email;
                        }
                        break;
                }
            }

            //foreach (var entry in ChangeTracker.Entries<IAuditTrail_Entity>())
            //{
            //    switch (entry.State)
            //    {
            //        case EntityState.Added:
            //            Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            //            Console.WriteLine("Added");
            //            Console.WriteLine(entry.Entity.ToJsonString());
            //            Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            //            break;
            //        case EntityState.Modified:
            //            Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            //            Console.WriteLine("Modified");
            //            Console.WriteLine(entry.Entity.ToJsonString());
            //            Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            //            //}
            //            break;
            //    }
            //}
        }

        public void Reset()
        {
            var entries = base.ChangeTracker
                                 .Entries()
                                 .Where(e => e.State != EntityState.Unchanged)
                                 .ToArray();

            foreach (var entry in entries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

        /// <summary>
        /// membangun conection string DAPPER
        /// </summary>
        protected void DapperInit(string callerName)
        {
            var db = _config.Database.FirstOrDefault(w => w.DbContext.ToLower() == callerName.ToLower());

            if (db == null)
            {
                throw new Unknown_Exception("DapperInit ERROR");
            }

            if (db.Provider == ProviderDatabase_Type.Sqlserver)
            {
                _dapperConstring = $"Server={db.Host},{db.Port};Database={db.DatabaseName};User Id={db.DatabaseUsername};Password={db.DatabasePassword};";
            }


        }

        public IDbConnection Dapper()
        {

            if (_provider == ProviderDatabase_Type.Postgresql)
            {
                return new NpgsqlConnection(_dapperConstring);
            }

            if (_provider == ProviderDatabase_Type.Sqlserver)
            {
                return new SqlConnection(_dapperConstring);
            }

            return default;


        }


    }
}
