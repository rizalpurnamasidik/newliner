﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.CompilerServices;
//using System.Text;
//using System.Threading.Tasks;
//using Apache.Ignite.Core.Client;
//using Application.Interfaces;
//using Microsoft.Extensions.Options;
//using RabbitMQ.Client;
//using Shared.Extensions;

//namespace Infrastructure.Databases
//{
//    public class ApacheIgnite
//    {
//        private readonly InfrastructureSettings _options;

//        public ApacheIgnite(IOptions<InfrastructureSettings> options)
//        {
//            _options = options.Value;
//        }

//        public IgniteClientConfiguration Connect()
//        {
//            var caller = ClassExtension.NameOfCallingClass();
//            var config = _options.Database.Where(w => w.DbContext.ToLower() == caller.ToLower()).FirstOrDefault();
//            var constring = new[] { $@"{config.Host}:{config.Port}" };

//            return new IgniteClientConfiguration { Endpoints = constring };
//        }

//    }
//}
