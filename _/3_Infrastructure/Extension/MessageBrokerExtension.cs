﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared.Constants;

namespace Infrastructure.Extension;

public static class MessageBrokerExtension
{
    public static IServiceCollection MessageBrokerConnect<TIMessageBroker, TMessageBroker>(this IServiceCollection services, ConfigurationManager configuration)
    where TMessageBroker : TIMessageBroker
    where TIMessageBroker : class
    {
        var settings = configuration.Get<InfrastructureSettings>();
        var mb = settings.MessageBroker.FirstOrDefault(w => w.Name.ToLower().Trim() == typeof(TMessageBroker).Name.ToLower());

        if (mb is null)
        { return services; }

        if (mb.Provider == ProviderMessageBroker_Type.Rabbitmq)
        {
            services.AddSingleton(typeof(TIMessageBroker), typeof(TMessageBroker));
        }

        return services;
    }
}
