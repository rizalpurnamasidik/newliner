﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.DataSources;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared.Constants;
using Shared.Extensions;

namespace Infrastructure.Extension
{
    public static class DatabaseExtension
    {
        public static IServiceCollection DbConnect<ITContext, TContext>(this IServiceCollection services, ConfigurationManager configuration) where TContext : DbContext, ITContext where ITContext : class
        {
            var settings = configuration.Get<InfrastructureSettings>();
            var contextName = typeof(TContext).Name;
            var db = settings.Database.FirstOrDefault(w => w.DbContext.ToLower().Trim() == typeof(TContext).Name.ToLower());
            var conString = "";

            if (db is null)
            { return services; }

            if (db.Provider == ProviderDatabase_Type.Sqlserver)
            {
                conString = $"Server={db.Host},{db.Port};Database={db.DatabaseName};User Id={db.DatabaseUsername};Password={db.DatabasePassword};";
                services.AddDbContext<TContext>(options => { options.UseSqlServer(conString); });
                services.AddScoped<ITContext>(provider => provider.GetService<TContext>());
            }

            if (db.Provider == ProviderDatabase_Type.Postgresql)
            {
                conString = $"User ID={db.DatabaseUsername};Password={db.DatabasePassword};Host={db.Host};Port={db.Port};Database={db.DatabaseName};";
                services.AddDbContext<TContext>(options => { options.UseNpgsql(conString); });
                services.AddScoped<ITContext>(provider => provider.GetService<TContext>());
            }

            if (db.Provider == ProviderDatabase_Type.InMemory)
            {
                services.AddDbContext<TContext>(options => { options.UseInMemoryDatabase(db.DatabaseUsername); });
                services.AddScoped<ITContext>(provider => provider.GetService<TContext>());
            }


            // if dbContext is IDENTITY
            if (typeof(TContext).BaseType.Name == typeof(IdentityDatabaseContext).Name)
            {
                services.AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    //options.Password.RequireDigit = settings.IdentityUser.Options.Password.RequireDigit;
                    //options.Password.RequireLowercase = settings.IdentityUser.Options.Password.RequireLowercase;
                    //options.Password.RequiredLength = settings.IdentityUser.Options.Password.RequiredLength;
                    //options.Password.RequireNonAlphanumeric = settings.IdentityUser.Options.Password.RequireNonAlphanumeric;
                    //options.Password.RequireUppercase = settings.IdentityUser.Options.Password.RequireUppercase;

                    //options.SignIn.RequireConfirmedAccount = settings.IdentityUser.Options.SignIn.RequireConfirmedAccount;
                    //options.SignIn.RequireConfirmedEmail = settings.IdentityUser.Options.SignIn.RequireConfirmedEmail;
                    options.SignIn.RequireConfirmedEmail = false;
                    //options.SignIn.RequireConfirmedPhoneNumber = settings.IdentityUser.Options.SignIn.RequireConfirmedPhoneNumber;

                    options.Lockout.AllowedForNewUsers = false;
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(2);
                    options.Lockout.MaxFailedAccessAttempts = 3;

                }).AddEntityFrameworkStores<TContext>()
                 .AddDefaultTokenProviders();
            }

            return services;
        }

        public static IServiceCollection DbConnectOthers<ITContext, TContext>(this IServiceCollection services, ConfigurationManager configuration) //where TContext : DbContext, ITContext where ITContext : class
        {
            var settings = configuration.Get<InfrastructureSettings>();
            var contextName = typeof(TContext).Name;
            var db = settings.Database.FirstOrDefault(w => w.DbContext.ToLower().Trim() == typeof(TContext).Name.ToLower());

            if (db is null)
            { return services; }

            if (db.Provider == ProviderDatabase_Type.Ignite)
            {
                services.AddScoped(typeof(ITContext), typeof(TContext));
            }

            return services;
        }


    }
}
