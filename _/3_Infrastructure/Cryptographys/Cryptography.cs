﻿using Application.Interfaces.Cryptography;
using Microsoft.Extensions.Options;

namespace Infrastructure.Cryptographys
{
    public partial class Cryptography : ICryptography
    {
        private readonly InfrastructureSettings _config;

        public Cryptography(IOptions<InfrastructureSettings> config)
        {
            _config = config.Value;
        }
    }
}
