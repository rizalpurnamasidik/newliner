﻿using System.Security.Cryptography;
using System.Text;
using Shared.Constants;

namespace Infrastructure.Cryptographys
{
    public partial class Cryptography
    {
        public string AesEncrypt(string text)
        {

            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            var keys = Encoding.UTF8.GetBytes("abc");

            try
            {
                foreach (var item in _config.Cryptography)
                {
                    if (item.Provider == ProviderCryptography_Type.Aes)
                    {
                        keys = Encoding.UTF8.GetBytes(item.Key);
                    }
                }

                using var aesAlg = Aes.Create();
                using var encryptor = aesAlg.CreateEncryptor(keys, aesAlg.IV);
                using var msEncrypt = new MemoryStream();
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(text);
                }

                var iv = aesAlg.IV;

                var decryptedContent = msEncrypt.ToArray();

                var result = new byte[iv.Length + decryptedContent.Length];

                Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                var str = Convert.ToBase64String(result);
                var fullCipher = Convert.FromBase64String(str);
                return str;
            }
            catch (Exception ex)
            {
                throw;
            }


        }

        public string AesDecrypt(string value)
        {


            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            var keys = Encoding.UTF8.GetBytes("abc");
            try
            {
                foreach (var item in _config.Cryptography)
                {
                    if (item.Provider == ProviderCryptography_Type.Aes)
                    {
                        keys = Encoding.UTF8.GetBytes(item.Key);
                    }
                }

                value = value.Replace(" ", "+");
                var fullCipher = Convert.FromBase64String(value);

                var iv = new byte[16];
                var cipher = new byte[fullCipher.Length - iv.Length];

                Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
                Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, fullCipher.Length - iv.Length);

                using var aesAlg = Aes.Create();
                using var decryptor = aesAlg.CreateDecryptor(keys, iv);
                string result;
                using (var msDecrypt = new MemoryStream(cipher))
                {
                    using var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
                    using var srDecrypt = new StreamReader(csDecrypt);
                    result = srDecrypt.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
