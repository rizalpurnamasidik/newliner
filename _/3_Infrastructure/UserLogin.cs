﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Interfaces.Cryptography;
using Microsoft.AspNetCore.Http;
using Shared.Constants;
using Shared.Extensions;

namespace Infrastructure
{
    public class UserLogin : IUserLogin
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICryptography _cryptography;


        public UserLogin(IHttpContextAccessor httpContextAccessor
            , ICryptography cryptography)
        {
            _httpContextAccessor = httpContextAccessor;
            _cryptography = cryptography;
            _cryptography = cryptography;
        }

        public bool IsAuthenticated => _httpContextAccessor.HttpContext is not null && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;


        public string? JwtToken
        {
            get
            {

                var token = _httpContextAccessor.HttpContext.Request.Cookies["JwtToken"];
                if (token is null)
                {
                    return "";
                }
                return token;
            }

            set
            {

                if (value != null)
                {
                    // set token to cookies
                    var cookieOptions = new CookieOptions
                    {
                        Expires = DateTime.Now.AddHours(1),
                        HttpOnly = true,
                    };
                    _httpContextAccessor.HttpContext.Response.Cookies.Append("JwtToken", value, cookieOptions);
                }
            }

        }

        public string? RefreshToken
        {
            get
            {

                var token = _httpContextAccessor.HttpContext.Request.Cookies["RefreshToken"];
                if (token is null)
                {
                    return "";
                }
                return token;
            }

            set
            {

                if (value != null)
                {
                    // set token to cookies
                    var cookieOptions = new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(6),
                        HttpOnly = true,
                    };
                    _httpContextAccessor.HttpContext.Response.Cookies.Append("RefreshToken", value, cookieOptions);
                }
            }

        }


        public string Name
        {
            get
            {
                var cliamEmail = ClaimTypes.Name;

                if (_httpContextAccessor.HttpContext is null)
                { return "[SystemBackgroundJob]"; }
                if (_httpContextAccessor.HttpContext?.User?.FindFirstValue(cliamEmail) is null)
                { return "[Unknown_User]"; }
                var data = _httpContextAccessor.HttpContext?.User?.FindFirstValue(cliamEmail);
                return data;
            }

            set
            {

                if (value != null)
                {
                    //_httpContextAccessor.HttpContext.Response.Cookies.Append(
                    //    "E"
                    //    , _cryptography.AES_Encrypt(value)
                    //    , new CookieOptions
                    //    {
                    //        HttpOnly = true,
                    //        Expires = DateTime.UtcNow.AddDays(7),

                    //    });
                }
            }

        }


        public string? UserId
        {
            get
            {
                if (_httpContextAccessor.HttpContext == null)
                { return "NOT_LOGIN"; }
                var userId = _httpContextAccessor.HttpContext
                        .User.Claims
                        .FirstOrDefault(claim => claim.Type == ClaimTypes.Email);

                if (userId != null)
                { return userId.Value; }
                else
                { return "NOT_LOGIN"; }
            }


        }

        public string? Email
        {
            get
            {

                if (_httpContextAccessor.HttpContext == null)
                { return "NOT_LOGIN"; }
                var userId = _httpContextAccessor.HttpContext
                        .User.Claims
                        .FirstOrDefault(claim => claim.Type == Claim_Type.Email.ToDescription());

                if (userId != null)
                { return _cryptography.AesDecrypt(userId.Value); }
                else
                { return "NOT_LOGIN"; }
            }



        }

    }
}
