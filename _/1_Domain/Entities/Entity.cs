﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    /// <summary>
    /// sebagai kerangka dasar untuk semua enity<br/>
    /// sudah ada kolom ID sebagai primary key nya
    /// </summary>
    public abstract class Entity<T>
    {
        [Key]
        public T Id { get; set; }
    }
}
