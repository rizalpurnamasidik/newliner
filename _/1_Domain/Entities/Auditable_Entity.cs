﻿using Domain.Interfaces;

namespace Domain.Entities
{
    /// <summary>
    /// kerangka dasar untuk entty yang memiliki kolom kolom audit
    /// </summary>
    public abstract class Auditable_Entity<T> : Entity<Guid>, IAuditable_Entity
    {

        public bool? IsDeleted { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
    }
}
