﻿namespace Domain.Interfaces
{
    public interface IAuditable_Entity
    {
        DateTimeOffset? CreatedDate { get; set; } // tanggal data di buat
        string CreatedBy { get; set; } // data dibuat oleh siapa
        DateTimeOffset? ModifiedDate { get; set; } // tanggal data diubah
        string? ModifiedBy { get; set; } // data diubah oleh siapa
    }
}
