﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Constants;
using Shared.Extensions;

namespace Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]

    // -----------------------------------------------------------------
    // -----------------------------------------------------------------
    // gunakan ini jika class hanya bisa di akses oleh yang miliki hak akses
    // line 2
    // line 3
    // -----------------------------------------------------------------
    // -----------------------------------------------------------------


    // ** ** ** ** **
    // beberapa bagian terkecil
    // ** ** ** ** **



    // bagian terkecil

    public class AuthorizeAttribute : Attribute
    {
        public string MenuKey { get; }
        public string MenuAction { get; }

        public AuthorizeAttribute()
        {
        }

        //public AuthorizeAttribute(MenuKey menuKey, MenuAction menuAction)
        //{
        //    MenuKey = menuKey.ToDescription();
        //    MenuAction = menuAction.ToDescription();
        //}

        /// <summary>
        /// 
        /// </summary>
        public AuthorizeAttribute(string menuKey, string menuAction)
        {
            MenuKey = menuKey;
            MenuAction = menuAction;
        }
    }
}
