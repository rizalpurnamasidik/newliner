﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Shared.Extensions
{
    public static class Json_Extension
    {
        public static T ToJsonObject<T>(this string result, bool removeAllComment = false)
        {

            var options = new JsonSerializerOptions
            {
                Converters = { new JsonStringEnumConverter() },
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };

            if (result is null or "")
            { return default; }

            if (removeAllComment)
            {
                var blockComments = @"/\*(.*?)\*/";
                var lineComments = @"//(.*?)\r?\n";
                var strings = @"""((\\[^\n]|[^""\n])*)""";
                var verbatimStrings = @"@(""[^""]*"")+";

                result = Regex.Replace(result,
                        blockComments + "|" + lineComments + "|" + strings + "|" + verbatimStrings,
                        me =>
                        {
                            if (me.Value.StartsWith("/*") || me.Value.StartsWith("//"))
                            {
                                return me.Value.StartsWith("//") ? Environment.NewLine : "";
                            }
                            // Keep the literal strings
                            return me.Value;
                        },
                        RegexOptions.Singleline);
            }


            var rtn = JsonSerializer.Deserialize<T>(result, options);
            return rtn;
        }

        public static string ToJsonString(this object result)
        {
            var options = new JsonSerializerOptions
            {
                Converters = { new JsonStringEnumConverter() }
            };
            return JsonSerializer.Serialize(result, options);
        }

        public static bool IsValidJson(this string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput))
            { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {

                try
                {
                    var tmpObj = JsonValue.Parse(strInput);
                }
                catch (FormatException fex)
                {
                    //Invalid json format
                    //Console.WriteLine(fex);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    //Console.WriteLine(ex.ToString());
                    return false;
                }

                return true;


            }
            else
            {
                return false;
            }
        }


    }
}
