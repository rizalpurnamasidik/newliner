﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;
using Shared.Requests;
using Shared.Constants;
using Shared.Exceptions;

namespace Shared.Extensions
{
    public static class Http_Extension
    {

        private static HttpClient _client;

        public static void HttpExtensionConfigure(HttpClient client)
        {
            _client = client;
        }

        public static async Task<ResponseBuilder<T>> PostAsync<T>(this Base_Request dataToSend, string url)
        {
            return await SendAsync<T>(dataToSend, url, HttpMethod.Post);
        }

        public static async Task<ResponseBuilder<T>> PutAsync<T>(this Base_Request dataToSend, string url)
        {
            return await SendAsync<T>(dataToSend, url, HttpMethod.Put);
        }

        //public static async Task<ResponseBuilder<T>> PutWithFile<T>(this BaseRequest dataToSend, string url, MultipartFormDataContent multipartForm)
        //{
        //    return await SendWithFile<T>(dataToSend, url, HttpMethod.Put, multipartForm);
        //}

        public static async Task<ResponseBuilder<T>> GetAsync<T>(this Base_Request dataToSend, string url, List<KeyValuePair<string, string>> cookies = null)
        {
            var result = new ResponseBuilder<T>();
            //var token = await GetToken();

            //try
            //{ // first trial. 
            //    result = await SendAsync<T>(dataToSend, url, HttpMethod.Get, token: token);
            //    return result;
            //}
            //catch (UnauthorizedAccessException ex)
            //{ // second trial : if the previouse failed, refresh token and try again
            //    await RefreshToken(token);
            //    token = await GetToken();
            //    result = await SendAsync<T>(dataToSend, url, HttpMethod.Get, false, token);
            //    return result;
            //}

            result = await SendAsync<T>(dataToSend, url, HttpMethod.Get, cookies);
            return result;
        }


        public static async Task<ResponseBuilder<T>> DeleteAsync<T>(this Base_Request dataToSend, string url)
        {
            return await SendAsync<T>(dataToSend, url, HttpMethod.Delete);
        }

        private static async Task<ResponseBuilder<T>> SendAsync<T>(Base_Request dataToSend, string url, HttpMethod httpMethod, List<KeyValuePair<string, string>> cookies = null)
        {
            var response = new HttpResponseMessage();

            HttpContent httpContent = dataToSend.ToJsonString().HttpStringContentJson();


            try
            {
                var request = new HttpRequestMessage(httpMethod, url);
                using var formData = new MultipartFormDataContent();

                // send cookies
                if (cookies is not null)
                {
                    var cookiesString = new StringBuilder();
                    foreach (var item in cookies)
                    {
                        request.Headers.Add(item.Key, item.Value);
                        cookiesString.Append($"{item.Key}={item.Value};");
                    }

                    request.Headers.Add("Cookie", cookiesString.ToString());
                }

                //formData.Add(httpContent);
                //request.Content = httpContent;
                //var requestzz = new HttpRequestMessage(httpMethod, url)
                //{
                //    Content = httpContent
                //};


                // get all data request //
                //var formVariables = new List<KeyValuePair<string, string>>();
                //foreach (var prop in dataToSend.GetType().GetProperties())
                //{
                //    if (prop.GetValue(dataToSend, null) != null)
                //    {
                //        // get type of object //
                //        var type = prop.GetValue(dataToSend).GetType();

                //        // if string //
                //        if (type == typeof(string))
                //        {
                //            //formData.Add(new StringContent(prop.GetValue(dataToSend).ToString()), prop.Name);
                //            formVariables.Add(new KeyValuePair<string, string>(prop.Name, prop.GetValue(dataToSend).ToString()));

                //        }
                //        else
                //        {
                //            formVariables.Add(new KeyValuePair<string, string>(prop.Name, prop.GetValue(dataToSend)));
                //        }

                //        // if list<string> //
                //        //if (type == typeof(List<string>))
                //        //{
                //        //    var dataList = prop.GetValue(dataToSend) as List<string>;
                //        //    foreach (var item in dataList)
                //        //    {
                //        //        formData.Add(new StringContent(item), $"{prop.Name}[]");
                //        //    }
                //        //}

                //    }
                //}

                //var formContent = new FormUrlEncodedContent(formVariables);



                foreach (var prop in dataToSend.GetType().GetProperties())
                {
                    if (prop.GetValue(dataToSend, null) != null)
                    {
                        // get type of object //
                        var type = prop.GetValue(dataToSend).GetType();

                        if (type == typeof(List<string>))
                        {
                            var dataList = prop.GetValue(dataToSend) as List<string>;
                            foreach (var item in dataList)
                            {
                                formData.Add(new StringContent(item), $"{prop.Name}[]");
                            }

                        }
                        else if (type == typeof(Dictionary<string, Dictionary<string, bool>>))
                        {
                            var dataList = prop.GetValue(dataToSend) as Dictionary<string, Dictionary<string, bool>>;
                            foreach (var item in dataList)
                            {
                                foreach (var item2 in item.Value)
                                {
                                    formData.Add(new StringContent(item2.Value.ToString()), $"{prop.Name}[{item.Key}][{item2.Key}]");
                                }
                            }
                        }
                        else
                        {
                            formData.Add(new StringContent(prop.GetValue(dataToSend).ToString()), prop.Name);
                        }

                        // if list<string> //
                        //if (type == typeof(List<string>))
                        //{
                        //    var dataList = prop.GetValue(dataToSend) as List<string>;
                        //    foreach (var item in dataList)
                        //    {
                        //        formData.Add(new StringContent(item), $"{prop.Name}[]");
                        //    }
                        //}

                    }
                }

                if (httpMethod == HttpMethod.Get)
                {
                    //request.Content = httpContent;
                    response = await _client.SendAsync(request);
                }

                if (httpMethod == HttpMethod.Post)
                {
                    response = await _client.PostAsync(url, formData);
                }

                if (httpMethod == HttpMethod.Delete)
                {
                    response = await _client.DeleteAsync(url);
                }

                if (httpMethod == HttpMethod.Put)
                {
                    response = await _client.PutAsync(url, formData);
                }

                //var response = await _client.SendAsync(request);

                // 404
                if ((int)response.StatusCode == 404)
                {
                    throw new Http404_Exception();
                }

                // bad request
                if ((int)response.StatusCode == 400)
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }

                var content = await response.Content.ReadAsStringAsync();
                var contentObject = content.ToJsonObject<ResponseBuilder<T>>();

                // Unauthenticated
                if (contentObject.Error.IsError == true && contentObject.Error.ErrorType == Error_Type.Unauthenticated.ToDescription())
                {
                    throw new Unauthenticated_Exception(contentObject.Error.ErrorMessages);
                }

                // Unauthenticated
                if (contentObject.Error.IsError == true && contentObject.Error.ErrorType == Error_Type.Unknown.ToDescription())
                {
                    throw new Unknown_Exception(contentObject.Error.ErrorMessages);
                }

                return contentObject;
            }
            catch (UnauthorizedAccessException exUn)
            {
                throw exUn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //private static async Task<ResponseBuilder<T>> SendWithFile<T>(BaseRequest dataToSend, string url, HttpMethod httpMethod, MultipartFormDataContent multipartForm)
        //{
        //    var toastrOptions = new ToastrOptions
        //    {
        //        CloseButton = true,
        //        ShowDuration = 0,
        //        HideDuration = 0,
        //        TimeOut = 0,
        //        ExtendedTimeOut = 0,
        //        HideMethod = ToastrHideMethod.SlideUp,
        //        ShowMethod = ToastrShowMethod.SlideDown,
        //        Position = ToastrPosition.TopRight
        //    };

        //    try
        //    {
        //        using var formData = new MultipartFormDataContent();
        //        var request = new HttpRequestMessage(httpMethod, url);


        //        request.Headers.Add("Accept-Language", "id-ID");
        //        if (httpMethod != HttpMethod.Get)
        //        {
        //            request.Content = multipartForm;
        //        }

        //        var response = await _client.SendAsync(request);
        //        var content = await response.Content.ReadAsStringAsync();
        //        var contentObject = content.ToJsonDeserialize<ResponseBuilder<T>>();


        //        // return error karena validation
        //        if (contentObject?.IsError == true && contentObject?.ErrorType == ErrorType.Validation.GetDescription() && contentObject?.ErrorsMessage?.Count > 0)
        //        {
        //            await _toastrService.Error(contentObject.ErrorsMessage.ToString("<br/>"), toastrOptions);
        //            return contentObject;
        //        }

        //        // return error tidak diketahui
        //        if (contentObject?.IsError == true && contentObject?.ErrorType == ErrorType.Unknown.GetDescription())
        //        {
        //            toastrOptions.Position = ToastrPosition.TopFullWidth;
        //            await _toastrService.Error(contentObject.ErrorsMessage.ToString("<br/><br/>"), toastrOptions);
        //            return contentObject;
        //        }


        //        return contentObject;
        //    }
        //    catch (Exception ex)
        //    {
        //        toastrOptions.Position = ToastrPosition.TopFullWidth;
        //        await _toastrService.Error(ex.Message, toastrOptions);
        //        return default;
        //    }

        //}

        //private static async Task RefreshToken(GetTokenResponse token)
        //{

        //    try
        //    {
        //        var request = new HttpRequestMessage(HttpMethod.Post, $"{IdentityEndpoint.Identity.RefreshToken}")
        //        {
        //            Content = new RefreshTokenRequest
        //            {
        //                UserId = "180c63e1-67e0-4da5-a3e2-950733fe99eb",
        //                JwtToken = token.JwtToken,
        //                RefreshToken = token.RefreshToken,
        //            }.ToJson().HttpStringContentJson()
        //        };
        //        var response = await _client.SendAsync(request);
        //        var content = await response.Content.ReadAsStringAsync();
        //        var contentObject = content.ToJsonDeserialize<ResponseBuilder<RefreshTokenResponse>>();

        //        if (contentObject.IsError)
        //        {
        //            //RedirectTo("/FetchData");
        //            _navigationManager.NavigateTo("/login");
        //        }


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //private static async Task<GetTokenResponse> GetToken()
        //{

        //    var request = new HttpRequestMessage(HttpMethod.Post, $"{IdentityEndpoint.Identity.GetToken}")
        //    { };
        //    var response = await _client.SendAsync(request);
        //    var content = await response.Content.ReadAsStringAsync();
        //    var contentObject = content.ToJsonDeserialize<ResponseBuilder<GetTokenResponse>>();
        //    return contentObject.Data;
        //}
    }
}
