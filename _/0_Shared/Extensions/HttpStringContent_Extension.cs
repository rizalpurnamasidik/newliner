﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Extensions
{
    public static class HttpStringContent_Extension
    {

        public static StringContent HttpStringContentJson(this string data)
        {
            return new StringContent(data, Encoding.UTF8, "application/json");
        }
    }
}
