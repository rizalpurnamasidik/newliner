﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Shared.Extensions
{
    public static class Class_Extension
    {
        public static string NameOfCallingClass()
        {
            string fullName;
            Type declaringType;
            var skipFrames = 2;
            do
            {
                var method = new StackFrame(skipFrames, false).GetMethod();
                declaringType = method.DeclaringType;
                if (declaringType == null)
                {
                    return method.Name;
                }
                skipFrames++;
                fullName = declaringType.FullName;
            }
            while (declaringType.Module.Name.Equals("mscorlib.dll", StringComparison.OrdinalIgnoreCase));

            return fullName.Split(".").Last();
        }

        //    foreach (var sourceField in type.GetFields())
        //    {
        //        var targetField = type.GetField(sourceField.Name);
        //        targetField.SetValue(target, sourceField.GetValue(source));
        //    }


        public static void CopyAllTo<TSource, TTarget>(this TSource source, TTarget target)
        {
            var sourceType = typeof(TSource);
            var targetType = typeof(TTarget);


            // ulangi sebanyak property target
            foreach (var targetProp in targetType.GetProperties())
            {
                // cari property sumber dengan nama sama
                foreach (var sourceProp in sourceType.GetProperties())
                {
                    if (targetProp.Name == sourceProp.Name)
                    {
                        // set value to target
                        targetProp.SetValue(target, sourceProp.GetValue(source));
                    }
                }

            }
        }


    }
}
