﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Responses
{
    public abstract class Base_Response<TKey> : Base_Response
    {
        public TKey Id { get; set; }

    }

    public abstract class Base_Response
    {
    }
}
