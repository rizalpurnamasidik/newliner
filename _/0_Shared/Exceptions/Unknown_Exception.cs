﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Exceptions
{
    public class Unknown_Exception : Exception
    {
        public IEnumerable<string> ErrorsMessage { get; set; } = new List<string>();
        public Unknown_Exception(IEnumerable<string> errorsMessage) : base()
        {
            ErrorsMessage = errorsMessage;
        }

        public Unknown_Exception(string message) : base()
        {
            ErrorsMessage = new List<string> { message };
        }
    }
}
