﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Exceptions
{
    public class Unauthenticated_Exception : Exception
    {
        public IEnumerable<string> ErrorsMessage { get; set; } = new List<string>();
        public Unauthenticated_Exception(IEnumerable<string> errorsMessage) : base()
        {
            ErrorsMessage = errorsMessage;
        }

        public Unauthenticated_Exception(string message) : base()
        {
            ErrorsMessage = new List<string> { message };
        }

        public Unauthenticated_Exception() : base()
        {
            ErrorsMessage = new List<string> { };
        }
    }
}
