﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Exceptions
{
    public class NotFound_Exception : Exception
    {
        public IEnumerable<string> ErrorsMessage { get; set; } = new List<string>();
        public NotFound_Exception(IEnumerable<string> errorsMessage) : base()
        {
            ErrorsMessage = errorsMessage;
        }

        public NotFound_Exception() : base()
        {
            ErrorsMessage = new List<string> { "Data Not Found" };
        }

    }
}
