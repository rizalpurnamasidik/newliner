﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Exceptions
{
    public class Http404_Exception : Exception
    {
        public IEnumerable<string> ErrorsMessage { get; set; } = new List<string>();
        public Http404_Exception(IEnumerable<string> errorsMessage) : base()
        {
            ErrorsMessage = errorsMessage;
        }

        public Http404_Exception() : base()
        {
            ErrorsMessage = new List<string> { };
        }

    }
}
