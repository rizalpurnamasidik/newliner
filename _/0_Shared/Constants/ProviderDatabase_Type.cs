﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Constants
{
    public enum ProviderDatabase_Type
    {
        [Description("sqlserver")] Sqlserver,
        [Description("postgresql")] Postgresql,

        [Description("ignite")] Ignite,
        [Description("inmemory")] InMemory,
    }
}
