﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Constants
{
    public enum Claim_Type
    {
        [Description("__EMAIL__")] Email,
        [Description("__MENU_PRIVILEGE__")] MenuPrivilege,
        [Description("__JWT_TOKEN__")] JwtToken,
    }
}
