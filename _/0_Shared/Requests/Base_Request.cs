﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Requests
{
    public abstract class Base_Request
    {
    }

    public abstract class BasePaging_Request
    {
        public int Page { get; set; }
        public int Size { get; set; }
    }

    public abstract class Base_Request<TKey> : Base_Request
    {
        public TKey? Id { get; set; }

    }
}
