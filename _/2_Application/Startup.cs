﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Application.Behaviours;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public static class Startup
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            //// Behaviour : Order is important
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(Validation_Behaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(Authorization_Behaviour<,>));

            return services;

        }
    }
}
