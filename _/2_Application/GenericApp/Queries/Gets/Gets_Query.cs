﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Extensions;
using Application.Interfaces.Databases;
using AutoMapper;
using Dapper;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Shared.Requests;
using Shared.Responses;

namespace Application.GenericApp.Queries.Gets;

//public record ReadsQuery<TRequest, TResponse> : IRequest<ResponseBuilder<List<TResponse>>>
public record Gets_Query<TRequest, TResponse> : IRequest<ResponseBuilder<List<TResponse>>>
where TRequest : Base_Request
where TResponse : Base_Response
{

}

public class GetsQueryHandler<TDBContext, TEntity, TRequest, TResponse> : IRequestHandler<Gets_Query<TRequest, TResponse>, ResponseBuilder<List<TResponse>>>
    where TDBContext : IDatabaseContext
    where TEntity : Entity<Guid>
    where TRequest : Base_Request
    where TResponse : Base_Response
{
    private readonly IDatabaseContext _context;

    public GetsQueryHandler(TDBContext ctx)
    {
        _context = ctx;
    }

    public async Task<ResponseBuilder<List<TResponse>>> Handle(Gets_Query<TRequest, TResponse> request, CancellationToken cancellationToken)
    {

        var readData = Mapper_Extensions.AutoMapper<TEntity, TResponse, List<TResponse>>(
                _context.Instance.Set<TEntity>().ToList()
            );

        //        var readData = _context.Instance.Set<TEntity>().ToList()
        //.AutoMapper<TEntity, TResponse, List<TResponse>>(
        //        );

        return readData.ResponseRead();
    }
}



