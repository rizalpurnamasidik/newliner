﻿using Application.Extensions;
using Application.Interfaces.Databases;
using Domain.Entities;
using MediatR;
using Shared.Requests;
using Shared.Responses;

namespace Application.GenericApp.Commands.Add;

public record Add_Command<TRequest, TResponse> : IRequest<ResponseBuilder<TResponse>>
where TRequest : Base_Request
where TResponse : Base_Response
{
    public readonly TRequest Request;

    public Add_Command(TRequest request)
    {
        Request = request;
    }
}

public class AddCommandHandler<TDBContext, TEntity, TRequest, TResponse> : IRequestHandler<Add_Command<TRequest, TResponse>, ResponseBuilder<TResponse>>
    where TDBContext : IDatabaseContext
    where TEntity : Entity<Guid>
    where TRequest : Base_Request
    where TResponse : Base_Response
{
    private readonly IDatabaseContext _context;

    public AddCommandHandler(TDBContext ctx)
    {
        _context = ctx;
    }

    public async Task<ResponseBuilder<TResponse>> Handle(Add_Command<TRequest, TResponse> request, CancellationToken cancellationToken)
    {
        var dateCreate = request.Request.AutoMapper<TRequest, TEntity>();

        _context.Instance.Set<TEntity>().Add(dateCreate);
        _context.Instance.SaveChanges();

        var rtn = dateCreate.AutoMapper<TEntity, TResponse>();

        return rtn.ResponseRead();
    }
}



