﻿using Application.Extensions;
using Application.Interfaces.Databases;
using Domain.Entities;
using MediatR;
using Shared.Exceptions;
using Shared.Extensions;
using Shared.Requests;
using Shared.Responses;

namespace Application.GenericApp.Commands.Change;

public record Change_Command<TRequest, TResponse> : IRequest<ResponseBuilder<TResponse>>
where TRequest : Base_Request<Guid>
where TResponse : Base_Response
{
    public readonly TRequest Request;

    public Change_Command(TRequest request)
    {
        Request = request;
    }
}

public class ChangeCommandHandler<TDBContext, TEntity, TRequest, TResponse> : IRequestHandler<Change_Command<TRequest, TResponse>, ResponseBuilder<TResponse>>
    where TDBContext : IDatabaseContext
    where TEntity : Entity<Guid>
    where TRequest : Base_Request<Guid>
    where TResponse : Base_Response
{
    private readonly IDatabaseContext _context;

    public ChangeCommandHandler(TDBContext ctx)
    {
        _context = ctx;
    }

    public async Task<ResponseBuilder<TResponse>> Handle(Change_Command<TRequest, TResponse> request, CancellationToken cancellationToken)
    {
        // find by ID
        var dataToChange = _context.Instance.Set<TEntity>().FirstOrDefault(w => w.Id == request.Request.Id);

        // udpate model
        request.Request.CopyAllTo(dataToChange);

        if (dataToChange == null)
        {
            throw new BadRequest_Exception("data not found");
        }

        // update data
        _context.Instance.Update(dataToChange);
        _context.Instance.SaveChanges();


        var rtn = dataToChange.AutoMapper<TEntity, TResponse>();

        return rtn.ResponseRemove();
    }
}



