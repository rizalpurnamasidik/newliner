﻿using Application.Extensions;
using Application.Interfaces.Databases;
using Domain.Entities;
using MediatR;
using Shared.Exceptions;
using Shared.Requests;
using Shared.Responses;

namespace Application.GenericApp.Commands.Remove;

public record Remove_Command<TRequest, TResponse> : IRequest<ResponseBuilder<TResponse>>
where TRequest : Base_Request<Guid>
where TResponse : Base_Response
{
    public readonly TRequest Request;

    public Remove_Command(TRequest request)
    {
        Request = request;
    }
}

public class RemoveCommandHandler<TDBContext, TEntity, TRequest, TResponse> : IRequestHandler<Remove_Command<TRequest, TResponse>, ResponseBuilder<TResponse>>
    where TDBContext : IDatabaseContext
    where TEntity : Entity<Guid>
    where TRequest : Base_Request<Guid>
    where TResponse : Base_Response
{
    private readonly IDatabaseContext _context;

    public RemoveCommandHandler(TDBContext ctx)
    {
        _context = ctx;
    }

    public async Task<ResponseBuilder<TResponse>> Handle(Remove_Command<TRequest, TResponse> request, CancellationToken cancellationToken)
    {
        // find by ID
        var dataToRemove = _context.Instance.Set<TEntity>().FirstOrDefault(w => w.Id == request.Request.Id);

        if (dataToRemove == null)
        {
            throw new BadRequest_Exception("data not found");
        }

        // remove data
        _context.Instance.Remove(dataToRemove);
        _context.Instance.SaveChanges();


        var rtn = dataToRemove.AutoMapper<TEntity, TResponse>();

        return rtn.ResponseRemove();
    }
}



