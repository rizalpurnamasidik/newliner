﻿using FluentValidation;
using Shared.Requests;
using Shared.Responses;

namespace Application.GenericApp.Commands.Remove;

public class Remove_Validator<TRequest, TResponse> : AbstractValidator<Remove_Command<TRequest, TResponse>>
where TRequest : Base_Request<Guid>
where TResponse : Base_Response
{
    public Remove_Validator()
    {
        RuleFor(r => r.Request.Id).NotNull();
    }
}
