﻿using Application.Interfaces;
using MediatR;
using Shared.Attributes;
using Shared.Exceptions;
using System.Reflection;

namespace Application.Behaviours
{
    public class Authorization_Behaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly IUserLogin _userSession;
        //private readonly IJwtGenerator _jwtGenerator;
        //private readonly IIdentityDbContext _identityDbContext;

        //public AuthorizationBehaviour(IIdentity identity, IJwtGenerator jwtGenerator, IIdentityDbContext identityDbContext)
        public Authorization_Behaviour(IUserLogin userSession)
        {
            _userSession = userSession;
            //_identity = identity;
            //_jwtGenerator = jwtGenerator;
            //_identityDbContext = identityDbContext;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            //
            // dapatkan atribute dengan tipe AuthorizeAttribute
            var authorizeAttributes = request.GetType().GetCustomAttributes<AuthorizeAttribute>();

            //
            // memulai proses pengecekan authorize
            if (authorizeAttributes.Any())
            {
                if (!_userSession.IsAuthenticated)
                {

                    // cek dahulu ke refresh token. kalau benar benar ndak punya akses langsung tendang aja..
                    //var result = await RefreshToken();
                    //if (result == false)
                    //{
                    throw new Unauthenticated_Exception("Current User is not authenticated."); // TODO : hardcode
                    //}
                }

                var authorizeAttributesWithPolicies = authorizeAttributes.Where(a => a.MenuKey != null).ToList();

                foreach (var menuKey in authorizeAttributesWithPolicies.Select(a => a.MenuKey))
                {
                    // action
                    foreach (var actionName in authorizeAttributesWithPolicies.Select(a => a.MenuAction))
                    {
                        var menuWithAction = $"{menuKey}.{actionName}";
                        //var haveMenuAccess = _userSession.MenuPrivilege.Any(x => x.MenuKey.ToLower() == menuKey.ToLower() && x.MenuAction.ToLower().Contains(actionName.ToLower()));

                        //if (!haveMenuAccess)
                        //{
                        //    var errorMessage = $"{_userSession.Email} does not have the following permission: {menuWithAction}";
                        //    throw new UnauthorizedAccessException(errorMessage);
                        //}
                    }

                }
            }

            return await next();
        }

    }
}
