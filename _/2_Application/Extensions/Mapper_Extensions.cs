﻿using AutoMapper;

namespace Application.Extensions
{
    public static class Mapper_Extensions
    {
        public static TDestination AutoMapper<TSource, TDestination>(this object value)
        {
            var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<TSource, TDestination>()
               );

            var mapper = new Mapper(config);
            return mapper.Map<TDestination>(value);
        }


        public static TResult AutoMapper<TSource, TDestination, TResult>(this object value)
        {
            var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<TSource, TDestination>()
               );

            var mapper = new Mapper(config);
            return mapper.Map<TResult>(value);
        }
    }
}

