﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace Application.Interfaces.MessageBrokers
{
    public interface IRabbitMq
    {
        IConnection CreateChannel();
    }
}



