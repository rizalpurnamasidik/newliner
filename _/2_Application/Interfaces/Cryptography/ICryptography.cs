﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Cryptography
{
    public interface ICryptography
    {
        public string AesEncrypt(string text);
        public string AesDecrypt(string text);
    }
}
