﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserLogin
    {
        public string UserId { get; }
        bool IsAuthenticated { get; }
        string Email { get; }
        string Name { get; set; }
        string? JwtToken { get; set; }
    }
}
