﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace Application.Interfaces.HttpContext
{
    public interface IHttpContext
    {
        public string BaseUrl { get; set; }
        public string CorrelationId { get; set; }
    }
}



