﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Email
{

    public interface IEmail
    {
        public void Send(string emailContext, List<string> to, string subject, string body);
    }
}
