﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Application.Interfaces.Logs
{
    public interface ILog
    {
        public void LogInformation(string? message, params object?[] args);
        public void LogError(string? message, params object?[] args);
        public string ErrorId();
    }
}
