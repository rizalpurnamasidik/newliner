
- Menggunakan Gitflow (https://danielkummer.github.io/git-flow-cheatsheet/index.id_ID.html)


- how to Run :
    - set Project 4_Todo_Api menjadi Startup Project
    - karena database menggunakan in memory, jadi bisa langusng run


## 0) Others
* [ ] Membuat Solution Template dapat langsung digunakan tanpa harus clone dari git

## 1) Shared
* [X] Attributes
* [X] Constants
* [X] Exceptions
* [X] Extensions

<br /><br />
## 2) Domain
* [X] Entity

<br /><br />
## 3) Application
* [X] CQRS
* [X] standarisasi return
* [X] catch semua error
* [X] validation
* [ ] auto code
    * [X] add
    * [ ] adds
    * [ ] Get
    * [X] Gets
    * [X] change
    * [ ] changes
    * [X] delete
    * [ ] deletes
* [ ] multi language

<br /><br />
## 4) Infrastructure
* [ ] auto migration
* [X] Multi ORM
    * [X] EF Core
    * [X] Dapper
* [X] Database multi koneksi
    * [X] sql server
    * [X] inmemory
    * [X] postgres
* [X] Cryptographys
    * [X] AES
* [X] Logging
    * [X] Serilog
* [X] Entity Configuration
* [ ] Migration Database
* [ ] db seeds
     
 <br /><br />
 ## 5) Persentation
* [ ] Web API
* [ ] Console
* [ ] Desktop
* [ ] Grpc


## auturan
* [ ] penamaan file class
* [ ] penamaan class
* [ ] komentar lv. class, lv.method, lv.1, lv.2, lv.3
* [ ] susucan file untuk domain, application, infrasturtur, api