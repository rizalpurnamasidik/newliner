﻿using Basket_Api._.Protos;
using Basket_Application.Basket.Basket.Commands.Add_Basket;
using Basket_Application.Basket.Basket.Commands.Remove_Basket;
using Basket_Application.Basket.Basket.Queries.Get_Baskets;
using Grpc.Core;
using MediatR;

namespace Basket_Api.Grpcs
{
    public class Basket_Grpc : BasketProto.BasketProtoBase
    {
        private readonly IMediator _mediator;
        private readonly IHttpContextAccessor _httpContext;

        public Basket_Grpc(IMediator mediator, IHttpContextAccessor httpContext)
        {
            _mediator = mediator;
            _httpContext = httpContext;
        }

        public override async Task<_.Protos.Add_Basket_Response> Add_Basket(_.Protos.Add_Basket_Request request, ServerCallContext context)
        {
            //
            // save data
            var addResult = await _mediator.Send(new Add_Basket_Command
            {
                BookId = Guid.Parse(request.BookId),
                Quantity = request.Quantity,
                UnitPrice = request.UnitPrice,
            });


            //
            // build data
            var dataItem = new Add_Basket_Response_Item
            {
                Id = addResult.Data.Id.ToString()
            };

            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(addResult.Error.ErrorMessages);
            error.IsError = addResult.Error.IsError;
            error.ErrorType = addResult.Error.ErrorType ?? "";

            //
            // build response
            var response = new _.Protos.Add_Basket_Response
            {
                Message = addResult.Message ?? "",
                Error = error,
                Data = dataItem
            };

            return response;
        }

        public override async Task<_.Protos.Get_Baskets_Response> Get_Baskets(_.Protos.Get_Baskets_Request request, ServerCallContext context)
        {
            //
            // get data
            var data = await _mediator.Send(new Get_Baskets_Query
            {
            });

            //
            // build list data
            var dataItems = new List<Get_Baskets_Response_Item>();
            foreach (var item in data.Data)
            {
                var dataItem = new Get_Baskets_Response_Item
                {
                    BookId = item.BookId.ToString(),
                    UnitPrice = int.Parse(item.UnitPrice.ToString()),
                    Quantity = item.Quantity,
                    CreatedBy = item.CreatedBy,
                    Id = item.Id.ToString()
                };

                dataItems.Add(dataItem);
            }


            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(data.Error.ErrorMessages);
            error.IsError = data.Error.IsError;
            error.ErrorType = data.Error.ErrorType ?? "";

            //var response = new _.Protos.Get_Books_Response
            //{
            //    Message = data.Message,
            //    Error = error
            //};
            var response = new _.Protos.Get_Baskets_Response
            {
                Message = data.Message ?? "",
                Error = error
            };
            response.Data.Add(dataItems);

            return response;
        }


        public override async Task<_.Protos.Remove_Basket_Response> Remove_Basket(_.Protos.Remove_Basket_Request request, ServerCallContext context)
        {
            //
            // remove data
            var removeResult = await _mediator.Send(new Remove_Basket_Command
            {
                Id = Guid.Parse(request.Id),
            });

            //
            // build data
            var dataItem = new Remove_Basket_Response_Item
            {
                Id = removeResult.Data.Id.ToString()
            };

            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(removeResult.Error.ErrorMessages);
            error.IsError = removeResult.Error.IsError;
            error.ErrorType = removeResult.Error.ErrorType ?? "";

            //
            // build response
            var response = new _.Protos.Remove_Basket_Response
            {
                Message = removeResult.Message ?? "",
                Error = error,
                Data = dataItem
            };

            return response;
        }
    }
}
