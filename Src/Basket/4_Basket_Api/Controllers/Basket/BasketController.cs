﻿using Basket_Application.Basket.Basket.Commands.Add_Basket;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;

namespace Basket_Api.Controllers.Basket
{
    public class BasketController : ApiController
    {

        public BasketController()
        {
        }


        [HttpPost()]
        public async Task<ActionResult<ResponseBuilder<Add_Basket_Response>>> Add([FromForm] Add_Basket_Command request)
        {
            return await Mediator.Send(request);
        }

        //[HttpPost()]
        //public async Task<ActionResult<ResponseBuilder<Add_Book_Response>>> Add([FromBody] Add_Book_Command request)
        //{
        //    return await Mediator.Send(request);
        //}

        //[HttpDelete()]
        //public async Task<ActionResult<ResponseBuilder<Remove_Book_Response>>> Remove([FromBody] Remove_Book_Command request)
        //{
        //    return await Mediator.Send(request);
        //}

        //[HttpPut()]
        //public async Task<ActionResult<ResponseBuilder<Change_Book_Response>>> Change([FromBody] Change_Book_Command request)
        //{
        //    return await Mediator.Send(request);
        //}

        ////[HttpGet("/Books")]
        ////public async Task<ActionResult<ResponseBuilder<List<Get_Books_Response>>>> Gets()
        ////{
        ////    return await Mediator.Send(new Gets_Query<Get_Books_Request, Get_Books_Response>());
        ////}

        //[HttpGet("/Books")]
        //public async Task<ActionResult<ResponseBuilder<List<Get_Books_Response>>>> Gets([FromQuery] Get_Books_Query request)
        //{
        //    return await Mediator.Send(request);
        //}

        //[HttpGet("/BooksWithEncryption")]
        //public async Task<ActionResult<ResponseBuilder<List<Get_BooksWithEncryption_Response>>>> Gets([FromQuery] Get_BooksWithEncryption_Query request)
        //{
        //    return await Mediator.Send(request);
        //}

        //[HttpGet("/Decryption")]
        //public async Task<ActionResult<ResponseBuilder<Get_Decryption_Response>>> Gets([FromQuery] Get_Decryption_Query request)
        //{
        //    return await Mediator.Send(request);
        //}

    }
}
