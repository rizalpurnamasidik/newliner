﻿using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities;

namespace Basket_Domain.Basket
{
    [Table("Basket", Schema = "Basket")]
    public class Basket_Basket_Entity : Auditable_Entity<Guid>
    {
        public Guid? BookId { get; set; }
        //public string? BookTitle { get; set; }
        public decimal? UnitPrice { get; set; }
        public int Quantity { get; set; }
    }
}
