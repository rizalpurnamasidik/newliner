﻿using Application.Interfaces.Databases;
using Basket_Domain.Basket;
using Microsoft.EntityFrameworkCore;

namespace Basket_Application._.Interfaces
{

    public interface IBasket_DbContext : IDatabaseContext
    {
        public DbSet<Basket_Basket_Entity> Basket_Basket_Entities { get; set; }

    }
}
