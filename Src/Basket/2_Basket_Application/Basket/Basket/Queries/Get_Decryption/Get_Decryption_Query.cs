﻿using Application.Extensions;
using Application.Interfaces.Cryptography;
using Application.Interfaces.Logs;
using Dapper;
using HelloBook_Application._.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Responses;

namespace Basket_Application.Book.Book.Queries.Get_Decryption
{
    public class Get_Decryption_Query : Get_Decryption_Request, IRequest<ResponseBuilder<Get_Decryption_Response>>
    {

    }

    public class Handler : IRequestHandler<Get_Decryption_Query, ResponseBuilder<Get_Decryption_Response>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;
        private readonly ILog _log;
        private readonly ICryptography _cryptography;

        public Handler(
            IHelloBook_DbContext helloBookDb
            , ILog log
            , ICryptography cryptography
            )
        {
            _helloBookDb = helloBookDb;
            _log = log;
            _cryptography = cryptography;
        }

        public async Task<ResponseBuilder<Get_Decryption_Response>> Handle(Get_Decryption_Query request, CancellationToken cancellationToken)
        {

            var data = _cryptography.AesDecrypt(request.EncryptText);

            // return 
            return new Get_Decryption_Response { Text = data }.ResponseRead();
        }


    }
}
