﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Basket_Application.Book.Book.Queries.Get_Decryption
{
    public class Get_Decryption_Request : Base_Request
    {
        public string EncryptText { get; set; }
    }

}
