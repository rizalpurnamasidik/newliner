﻿using Application.Extensions;
using Application.Interfaces.Logs;
using Dapper;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Responses;

namespace Basket_Application.Book.Book.Queries.Get_Books
{
    public class Get_Books_Query : Get_Books_Request, IRequest<ResponseBuilder<List<Get_Books_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_Books_Query, ResponseBuilder<List<Get_Books_Response>>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;
        private readonly ILog _log;

        public Handler(
            IHelloBook_DbContext helloBookDb
            , ILog log
            )
        {
            _helloBookDb = helloBookDb;
            _log = log;
        }

        public async Task<ResponseBuilder<List<Get_Books_Response>>> Handle(Get_Books_Query request, CancellationToken cancellationToken)
        {

            //
            // get data with ef core
            var query = _helloBookDb.HelloBook_Book_Entities.AsQueryable();

            if (request.Title is not null)
            {
                query = query.Where(w => w.Title.Contains(request.Title));
            }

            // filter 
            if (request.Page > 0 && request.Size > 0)
            {
                query = query.Skip((request.Page - 1) * request.Size).Take(request.Size);
            }

            // execute query and mapping to response class
            var data = query
                    .Select(s => new Get_Books_Response
                    {
                        Id = s.Id,
                        Title = s.Title,
                        Author = s.Author,
                        CategoryId = s.CategoryId,

                    })
                .ToList();

            //
            // return 
            return data.ResponseRead();
        }


    }
}
