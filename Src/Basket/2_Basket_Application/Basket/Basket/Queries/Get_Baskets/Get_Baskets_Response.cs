﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Basket_Application.Basket.Basket.Queries.Get_Baskets
{
    public class Get_Baskets_Response : Base_Response<Guid>
    {
        public Guid? BookId { get; set; }
        public decimal? UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string CreatedBy { get; set; }
    }
}
