﻿using Application.Extensions;
using Application.Interfaces;
using Application.Interfaces.Cryptography;
using Application.Interfaces.Logs;
using Basket_Application._.Interfaces;
using MediatR;
using Shared.Responses;

namespace Basket_Application.Basket.Basket.Queries.Get_Baskets
{
    public class Get_Baskets_Query : Get_Baskets_Request, IRequest<ResponseBuilder<List<Get_Baskets_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_Baskets_Query, ResponseBuilder<List<Get_Baskets_Response>>>
    {
        private readonly IBasket_DbContext _basketDb;
        private readonly ILog _log;
        private readonly IUserLogin _userLogin;
        private readonly ICryptography _cryptography;

        public Handler(
            IBasket_DbContext basketDb
            , ILog log
            , IUserLogin userLogin
            , ICryptography cryptography
            )
        {
            _basketDb = basketDb;
            _log = log;
            _userLogin = userLogin;
            _cryptography = cryptography;
        }

        public async Task<ResponseBuilder<List<Get_Baskets_Response>>> Handle(Get_Baskets_Query request, CancellationToken cancellationToken)
        {
            //
            // get data with ef core
            var data = _basketDb.Basket_Basket_Entities
                    .Where(w => w.CreatedBy == _userLogin.Email)
                    .Select(s => new Get_Baskets_Response
                    {
                        Id = s.Id,
                        BookId = s.BookId,
                        Quantity = s.Quantity,
                        UnitPrice = s.UnitPrice,
                        CreatedBy = s.CreatedBy
                    })
                .ToList();

            //
            // return 
            return data.ResponseRead();
        }


    }
}
