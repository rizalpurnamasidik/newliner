﻿using FluentValidation;

namespace Basket_Application.Book.Book.Commands.Change_Book
{
    public class Change_Book_Validator : AbstractValidator<Change_Book_Command>
    {
        public Change_Book_Validator()
        {
            RuleFor(r => r.Id).NotEmpty();
            RuleFor(r => r.Title).NotEmpty();
        }
    }
}
