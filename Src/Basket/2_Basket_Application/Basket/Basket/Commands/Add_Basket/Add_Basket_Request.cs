﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Basket_Application.Basket.Basket.Commands.Add_Basket
{
    public class Add_Basket_Request : Base_Request
    {

        public Guid? BookId { get; set; }
        //public string? BookTitle { get; set; }
        public decimal? UnitPrice { get; set; }
        public int Quantity { get; set; }
    }

}
