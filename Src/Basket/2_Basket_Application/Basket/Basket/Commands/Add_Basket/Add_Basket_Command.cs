﻿using Application.Extensions;
using Basket_Application._.Interfaces;
using Basket_Domain.Basket;
using MediatR;
using Shared.Attributes;
using Shared.Responses;

namespace Basket_Application.Basket.Basket.Commands.Add_Basket
{
    //[Authorize()]
    public class Add_Basket_Command : Add_Basket_Request, IRequest<ResponseBuilder<Add_Basket_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_Basket_Command, ResponseBuilder<Add_Basket_Response>>
    {
        private readonly IBasket_DbContext _basketDb;

        public Handler(
           IBasket_DbContext basketDb
            )
        {
            _basketDb = basketDb;
        }

        public async Task<ResponseBuilder<Add_Basket_Response>> Handle(Add_Basket_Command request, CancellationToken cancellationToken)
        {
            // build data to save
            var dataToSave = new Basket_Basket_Entity
            {
                BookId = request.BookId,
                Quantity = request.Quantity,
                UnitPrice = request.UnitPrice,
            };

            // save data
            _basketDb.Basket_Basket_Entities.Add(dataToSave);
            await _basketDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Add_Basket_Response
            {
                Id = dataToSave.Id
            }.ResponseCreate();
        }


    }
}
