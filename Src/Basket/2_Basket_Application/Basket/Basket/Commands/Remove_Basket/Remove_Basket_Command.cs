﻿using Application.Extensions;
using Basket_Application._.Interfaces;
using Basket_Domain.Basket;
using MediatR;
using Shared.Attributes;
using Shared.Exceptions;
using Shared.Responses;

namespace Basket_Application.Basket.Basket.Commands.Remove_Basket
{
    //[Authorize()]
    public class Remove_Basket_Command : Remove_Basket_Request, IRequest<ResponseBuilder<Remove_Basket_Response>>
    {

    }

    public class Handler : IRequestHandler<Remove_Basket_Command, ResponseBuilder<Remove_Basket_Response>>
    {
        private readonly IBasket_DbContext _basketDb;

        public Handler(
           IBasket_DbContext basketDb
            )
        {
            _basketDb = basketDb;
        }

        public async Task<ResponseBuilder<Remove_Basket_Response>> Handle(Remove_Basket_Command request, CancellationToken cancellationToken)
        {
            // find data
            var dataToRemove = _basketDb.Basket_Basket_Entities.FirstOrDefault(x => x.Id == request.Id);

            // validation
            if (dataToRemove == null)
            {
                throw new NotFound_Exception();
            }

            // remove data
            _basketDb.Basket_Basket_Entities.Remove(dataToRemove);
            await _basketDb.SaveChangesAsync(cancellationToken);

            // return 
            return new Remove_Basket_Response
            {
                Id = dataToRemove.Id
            }.ResponseCreate();
        }


    }
}
