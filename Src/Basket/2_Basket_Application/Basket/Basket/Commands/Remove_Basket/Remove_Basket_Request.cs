﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Basket_Application.Basket.Basket.Commands.Remove_Basket
{
    public class Remove_Basket_Request : Base_Request
    {
        public Guid? Id { get; set; }
    }

}
