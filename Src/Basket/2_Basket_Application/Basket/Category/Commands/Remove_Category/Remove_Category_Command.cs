﻿using Application.Extensions;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using MediatR;
using Shared.Exceptions;
using Shared.Responses;

namespace Basket_Application.Book.Category.Commands.Remove_Category
{
    public class Remove_Category_Command : Remove_Category_Request, IRequest<ResponseBuilder<Remove_Category_Response>>
    {

    }

    public class Handler : IRequestHandler<Remove_Category_Command, ResponseBuilder<Remove_Category_Response>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;

        public Handler(
           IHelloBook_DbContext helloBookDb
            )
        {
            _helloBookDb = helloBookDb;
        }

        public async Task<ResponseBuilder<Remove_Category_Response>> Handle(Remove_Category_Command request, CancellationToken cancellationToken)
        {
            // find data
            var dataToRemove = _helloBookDb.HelloBook_Category_Entities.FirstOrDefault(x => x.Id == request.Id);

            // validation
            if (dataToRemove == null)
            {
                throw new NotFound_Exception();
            }

            // remove data
            _helloBookDb.HelloBook_Category_Entities.Remove(dataToRemove);
            await _helloBookDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Remove_Category_Response
            {
                Id = dataToRemove.Id
            }.ResponseRemove();
        }


    }
}
