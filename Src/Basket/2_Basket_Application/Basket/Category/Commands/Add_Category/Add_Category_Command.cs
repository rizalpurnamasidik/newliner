﻿using Application.Extensions;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using MediatR;
using Shared.Responses;

namespace Basket_Application.Book.Category.Commands.Add_Category
{
    public class Add_Category_Command : Add_Category_Request, IRequest<ResponseBuilder<Add_Category_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_Category_Command, ResponseBuilder<Add_Category_Response>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;

        public Handler(
           IHelloBook_DbContext helloBookDb
            )
        {
            _helloBookDb = helloBookDb;
        }

        public async Task<ResponseBuilder<Add_Category_Response>> Handle(Add_Category_Command request, CancellationToken cancellationToken)
        {
            //
            // build data to save
            var dataToSave = new HelloBook_Category_Entity
            {
                CategoryName = request.CategoryName,
            };

            //
            // save data
            _helloBookDb.HelloBook_Category_Entities.Add(dataToSave);
            await _helloBookDb.SaveChangesAsync(cancellationToken);

            //
            // return 
            return new Add_Category_Response
            {
                Id = dataToSave.Id
            }.ResponseCreate();
        }


    }
}
