﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure;
using Infrastructure.Extension;
using Basket_Infrastructure.Settings;
using Basket_Application._.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Basket_Infrastructure.DataSources.Basket;

namespace Basket_Infrastructure
{
    public static class Startup
    {
        public static IServiceCollection AddBasketInfrastructure(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddInfrastructure();
            services.LoadSettings<BasketSettings>(configuration);

            services.AddDbContext(configuration, db =>
            {
                db.DbConnect<IBasket_DbContext, Basket_DbContext>(configuration);
            });

            // /// //// /////

            var settings = configuration.Get<InfrastructureSettings>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                            .GetBytes(settings.Jwt.Secret)),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                });


            return services;
        }
    }
}
