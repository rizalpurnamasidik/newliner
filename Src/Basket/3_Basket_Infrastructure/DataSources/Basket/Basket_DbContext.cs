﻿using Application.Interfaces;
using Basket_Application._.Interfaces;
using Basket_Domain.Basket;
using Basket_Infrastructure.Settings;
using Infrastructure.DataSources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Basket_Infrastructure.DataSources.Basket
{

    public class Basket_DbContext : DatabaseContext, IBasket_DbContext
    {

        public DbContext Instance => this;

        public Basket_DbContext(DbContextOptions<Basket_DbContext> options,
            IOptions<BasketSettings> config
            , IUserLogin userLogin
            ) : base(options, config, userLogin)
        {
            DapperInit(nameof(Basket_DbContext));
        }

        public DbSet<Basket_Basket_Entity> Basket_Basket_Entities { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Basket_Basket_Entity>().Property(b => b.Id).HasDefaultValueSql("uuid_generate_v4()");
        }
    }
}
