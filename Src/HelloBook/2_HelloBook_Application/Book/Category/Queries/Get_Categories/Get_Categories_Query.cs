﻿using Application.Extensions;
using Application.Interfaces.Logs;
using HelloBook_Application._.Interfaces;
using MediatR;
using Shared.Responses;

namespace HelloBook_Application.Book.Category.Queries.Get_Categories
{
    public class Get_Categories_Query : Get_Categories_Request, IRequest<ResponseBuilder<List<Get_Categories_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_Categories_Query, ResponseBuilder<List<Get_Categories_Response>>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;
        private readonly ILog _log;

        public Handler(
            IHelloBook_DbContext helloBookDb
            , ILog log
            )
        {
            _helloBookDb = helloBookDb;
            _log = log;
        }

        public async Task<ResponseBuilder<List<Get_Categories_Response>>> Handle(Get_Categories_Query request, CancellationToken cancellationToken)
        {


            //
            //
            var data = _helloBookDb.HelloBook_Category_Entities
                    .Select(s => new Get_Categories_Response
                    {
                        Id = s.Id,
                        CategoryName = s.CategoryName,
                    })
                .ToList();

            //
            // return 
            return data.ResponseRead();
        }


    }
}
