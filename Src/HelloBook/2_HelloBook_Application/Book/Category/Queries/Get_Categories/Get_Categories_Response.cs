﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace HelloBook_Application.Book.Category.Queries.Get_Categories
{
    public class Get_Categories_Response : Base_Response<Guid>
    {
        public string? CategoryName { get; set; } = string.Empty;

    }
}
