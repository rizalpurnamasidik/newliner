﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace HelloBook_Application.Book.Category.Commands.Remove_Category
{
    public class Remove_Category_Request : Base_Request
    {
        public Guid? Id { get; set; }
    }

}
