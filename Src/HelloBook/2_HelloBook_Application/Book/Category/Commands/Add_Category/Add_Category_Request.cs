﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace HelloBook_Application.Book.Category.Commands.Add_Category
{
    public class Add_Category_Request : Base_Request
    {
        public string? CategoryName { get; set; }
    }

}
