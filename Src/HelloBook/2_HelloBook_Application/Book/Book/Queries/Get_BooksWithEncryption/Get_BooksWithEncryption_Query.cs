﻿using Application.Extensions;
using Application.Interfaces.Cryptography;
using Application.Interfaces.Logs;
using Dapper;
using HelloBook_Application._.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Queries.Get_BooksWithEncryption
{
    public class Get_BooksWithEncryption_Query : Get_BooksWithEncryption_Request, IRequest<ResponseBuilder<List<Get_BooksWithEncryption_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_BooksWithEncryption_Query, ResponseBuilder<List<Get_BooksWithEncryption_Response>>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;
        private readonly ILog _log;
        private readonly ICryptography _cryptography;

        public Handler(
            IHelloBook_DbContext helloBookDb
            , ILog log
            , ICryptography cryptography
            )
        {
            _helloBookDb = helloBookDb;
            _log = log;
            _cryptography = cryptography;
        }

        public async Task<ResponseBuilder<List<Get_BooksWithEncryption_Response>>> Handle(Get_BooksWithEncryption_Query request, CancellationToken cancellationToken)
        {

            // get data with ef core
            var data = await _helloBookDb.HelloBook_Book_Entities
                .Select(s => new Get_BooksWithEncryption_Response
                {
                    Id = _cryptography.AesEncrypt(s.Id.ToString()),
                    Title = _cryptography.AesEncrypt(s.Title),
                    Author = _cryptography.AesEncrypt(s.Author),
                    CategoryId = s.CategoryId.HasValue ? _cryptography.AesEncrypt(s.CategoryId.ToString()) : null,

                })
                .ToListAsync();

            // return 
            return data.ResponseRead();
        }


    }
}
