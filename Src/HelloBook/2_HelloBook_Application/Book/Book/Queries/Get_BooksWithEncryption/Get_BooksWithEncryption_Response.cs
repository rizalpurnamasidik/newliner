﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Queries.Get_BooksWithEncryption
{
    public class Get_BooksWithEncryption_Response : Base_Response<string>
    {
        public string? CategoryId { get; set; }
        public string? Title { get; set; } = string.Empty;
        public string? Author { get; set; } = string.Empty;
    }
}
