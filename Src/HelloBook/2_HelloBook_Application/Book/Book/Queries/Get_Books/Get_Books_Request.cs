﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace HelloBook_Application.Book.Book.Queries.Get_Books
{
    public class Get_Books_Request : BasePaging_Request //Base_Request
    {
        public string Title { get; set; }
    }

}
