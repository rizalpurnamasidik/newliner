﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Queries.Get_Decryption
{
    public class Get_Decryption_Response : Base_Response
    {
        public string? Text { get; set; }
    }
}
