﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Queries.Get_CatalogBooks
{
    public class Get_CatalogBooks_Response : Base_Response<Guid>
    {
        public Guid? CategoryId { get; set; }
        public string? Title { get; set; } = string.Empty;
        public string? Author { get; set; } = string.Empty;
        public int UnitPrice { get; set; }
    }
}
