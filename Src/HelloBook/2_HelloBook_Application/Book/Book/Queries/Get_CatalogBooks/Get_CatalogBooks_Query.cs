﻿using Application.Extensions;
using Application.Interfaces.Logs;
using Dapper;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Queries.Get_CatalogBooks
{
    public class Get_CatalogBooks_Query : Get_CatalogBooks_Request, IRequest<ResponseBuilder<List<Get_CatalogBooks_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_CatalogBooks_Query, ResponseBuilder<List<Get_CatalogBooks_Response>>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;
        private readonly ILog _log;

        public Handler(
            IHelloBook_DbContext helloBookDb
            , ILog log
            )
        {
            _helloBookDb = helloBookDb;
            _log = log;
        }

        public async Task<ResponseBuilder<List<Get_CatalogBooks_Response>>> Handle(Get_CatalogBooks_Query request, CancellationToken cancellationToken)
        {

            //
            // get data with ef core
            var data = _helloBookDb.HelloBook_Book_Entities
                    .Select(s => new Get_CatalogBooks_Response
                    {
                        Id = s.Id,
                        Title = s.Title,
                        Author = s.Author,
                        CategoryId = s.CategoryId,
                        UnitPrice = s.UnitPrice
                    })
                .ToList();

            //
            // return 
            return data.ResponseRead();
        }


    }
}
