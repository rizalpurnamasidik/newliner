﻿using FluentValidation;

namespace HelloBook_Application.Book.Book.Commands.Remove_Book
{
    public class Remove_Book_Validator : AbstractValidator<Remove_Book_Command>
    {
        public Remove_Book_Validator()
        {
            RuleFor(r => r.Id).NotEmpty();
        }
    }
}
