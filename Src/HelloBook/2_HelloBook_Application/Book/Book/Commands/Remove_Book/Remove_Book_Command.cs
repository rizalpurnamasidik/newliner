﻿using Application.Extensions;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using MediatR;
using Shared.Exceptions;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Commands.Remove_Book
{
    public class Remove_Book_Command : Remove_Book_Request, IRequest<ResponseBuilder<Remove_Book_Response>>
    {

    }

    public class Handler : IRequestHandler<Remove_Book_Command, ResponseBuilder<Remove_Book_Response>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;

        public Handler(
           IHelloBook_DbContext helloBookDb
            )
        {
            _helloBookDb = helloBookDb;
        }

        public async Task<ResponseBuilder<Remove_Book_Response>> Handle(Remove_Book_Command request, CancellationToken cancellationToken)
        {
            // find data
            var dataToRemove = _helloBookDb.HelloBook_Book_Entities.FirstOrDefault(x => x.Id == request.Id);

            // validation
            if (dataToRemove == null)
            {
                throw new NotFound_Exception();
            }

            // remove data
            _helloBookDb.HelloBook_Book_Entities.Remove(dataToRemove);
            await _helloBookDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Remove_Book_Response
            {
                Id = dataToRemove.Id
            }.ResponseRemove();
        }


    }
}
