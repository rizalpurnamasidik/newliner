﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace HelloBook_Application.Book.Book.Commands.Remove_Book
{
    public class Remove_Book_Request : Base_Request
    {
        public Guid? Id { get; set; }
    }

}
