﻿using FluentValidation;

namespace HelloBook_Application.Book.Book.Commands.Add_Book
{
    public class Add_Book_Validator : AbstractValidator<Add_Book_Command>
    {
        public Add_Book_Validator()
        {
            RuleFor(r => r.Title).NotEmpty();
            RuleFor(r => r.Author).NotEmpty();
        }
    }
}
