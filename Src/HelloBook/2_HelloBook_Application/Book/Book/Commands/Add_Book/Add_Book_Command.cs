﻿using Application.Extensions;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using MediatR;
using Shared.Attributes;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Commands.Add_Book
{
    //[Authorize()]
    public class Add_Book_Command : Add_Book_Request, IRequest<ResponseBuilder<Add_Book_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_Book_Command, ResponseBuilder<Add_Book_Response>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;

        public Handler(
           IHelloBook_DbContext helloBookDb
            )
        {
            _helloBookDb = helloBookDb;
        }

        public async Task<ResponseBuilder<Add_Book_Response>> Handle(Add_Book_Command request, CancellationToken cancellationToken)
        {
            // build data to save
            var dataToSave = new HelloBook_Book_Entity
            {
                Author = request.Author,
                Title = request.Title,
                UnitPrice = request.UnitPrice,
            };

            // save data
            _helloBookDb.HelloBook_Book_Entities.Add(dataToSave);
            await _helloBookDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Add_Book_Response
            {
                Id = dataToSave.Id
            }.ResponseCreate();
        }


    }
}
