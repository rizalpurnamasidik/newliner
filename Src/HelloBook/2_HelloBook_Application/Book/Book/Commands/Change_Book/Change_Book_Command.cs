﻿using Application.Extensions;
using HelloBook_Application._.Interfaces;
using MediatR;
using Shared.Exceptions;
using Shared.Extensions;
using Shared.Responses;

namespace HelloBook_Application.Book.Book.Commands.Change_Book
{
    public class Change_Book_Command : Change_Book_Request, IRequest<ResponseBuilder<Change_Book_Response>>
    {

    }

    public class Handler : IRequestHandler<Change_Book_Command, ResponseBuilder<Change_Book_Response>>
    {
        private readonly IHelloBook_DbContext _helloBookDb;

        public Handler(
           IHelloBook_DbContext helloBookDb
            )
        {
            _helloBookDb = helloBookDb;
        }

        public async Task<ResponseBuilder<Change_Book_Response>> Handle(Change_Book_Command request, CancellationToken cancellationToken)
        {
            // find data
            var dataToChange = _helloBookDb.HelloBook_Book_Entities.FirstOrDefault(x => x.Id == request.Id);

            // validation
            if (dataToChange == null)
            {
                throw new NotFound_Exception();
            }

            // change data
            request.CopyAllTo(dataToChange);

            _helloBookDb.HelloBook_Book_Entities.Update(dataToChange);
            await _helloBookDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Change_Book_Response
            {
                Id = dataToChange.Id
            }.ResponseChange();
        }


    }
}
