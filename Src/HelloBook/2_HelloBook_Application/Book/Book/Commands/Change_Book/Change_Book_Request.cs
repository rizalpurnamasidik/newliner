﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace HelloBook_Application.Book.Book.Commands.Change_Book
{
    public class Change_Book_Request : Base_Request
    {
        public Guid? Id { get; set; }
        public string? Title { get; set; } = string.Empty;
    }

}
