﻿using System.Reflection;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Application;
using Application.Behaviours;
using Application.GenericApp.Queries.Gets;
using HelloBook_Application.Book.Book.Queries.Get_Books;
using Shared.Responses;
using HelloBook_Domain.HelloBook;
using HelloBook_Application._.Interfaces;
using Application.GenericApp.Commands.Add;
using HelloBook_Application.Book.Book.Commands.Add_Book;

namespace HelloBook_Application
{
    public static class Startup
    {
        public static IServiceCollection AddHelloBookApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            //services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddApplication();
            services.GenericApp();

            //// Behaviour : Order is important
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(Validation_Behaviour<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthorizationBehaviour<,>));

            return services;

        }

        /// <summary>
        /// untuk proses CRUD yang standar
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection GenericApp(this IServiceCollection services)
        {
            //
            // book
            //services.AddTransient(typeof(IRequestHandler<Gets_Query<Get_Books_Request, Get_Books_Response>, ResponseBuilder<List<Get_Books_Response>>>)
            //    , typeof(GetsQueryHandler<IHelloBook_DbContext, HelloBook_Book_Entity, Get_Books_Request, Get_Books_Response>));

            //services.AddTransient(typeof(IRequestHandler<Add_Command<Add_Book_Request, Add_Book_Response>, ResponseBuilder<Add_Book_Response>>)
            //    , typeof(AddCommandHandler<IHelloBook_DbContext, HelloBook_Book_Entity, Add_Book_Request, Add_Book_Response>));

            //services.AddTransient(typeof(IRequestHandler<Remove_Command<Remove_List_Request, Remove_List_Response>, ResponseBuilder<Remove_List_Response>>)
            //    , typeof(RemoveCommandHandler<ITodoDbContext, Todo_List_Entity, Remove_List_Request, Remove_List_Response>));

            //services.AddTransient(typeof(IRequestHandler<Change_Command<Change_List_Request, Change_List_Response>, ResponseBuilder<Change_List_Response>>)
            //    , typeof(ChangeCommandHandler<ITodoDbContext, Todo_List_Entity, Change_List_Request, Change_List_Response>));


            //
            // todo item
            //services.AddTransient(typeof(IRequestHandler<GetsQuery<Get_Todo_Items_Request, Get_Todo_Items_Response>, ResponseBuilder<List<Get_Todo_Items_Response>>>)
            //    , typeof(GetsQueryHandler<ITodoDbContext, Todo_Item_Entity, Get_Todo_Items_Request, Get_Todo_Items_Response>));

            //services.AddTransient(typeof(IRequestHandler<AddCommand<Add_Todo_Item_Request, Add_Todo_Item_Response>, ResponseBuilder<Add_Todo_Item_Response>>)
            //    , typeof(AddCommandHandler<ITodoDbContext, Todo_Item_Entity, Add_Todo_Item_Request, Add_Todo_Item_Response>));

            //services.AddTransient(typeof(IRequestHandler<RemoveCommand<Remove_Todo_Item_Request, Remove_Todo_Item_Response>, ResponseBuilder<Remove_Todo_Item_Response>>)
            //    , typeof(RemoveCommandHandler<ITodoDbContext, Todo_Item_Entity, Remove_Todo_Item_Request, Remove_Todo_Item_Response>));

            //services.AddTransient(typeof(IRequestHandler<ChangeCommand<Change_Todo_Item_Request, Change_Todo_Item_Response>, ResponseBuilder<Change_Todo_Item_Response>>)
            //    , typeof(ChangeCommandHandler<ITodoDbContext, Todo_Item_Entity, Change_Todo_Item_Request, Change_Todo_Item_Response>));



            return services;
        }



    }
}
