﻿using Application.Interfaces.Databases;
using HelloBook_Domain.HelloBook;
using Microsoft.EntityFrameworkCore;

namespace HelloBook_Application._.Interfaces
{

    public interface IHelloBook_DbContext : IDatabaseContext
    {
        public DbSet<HelloBook_Book_Entity> HelloBook_Book_Entities { get; set; }
        public DbSet<HelloBook_Category_Entity> HelloBook_Category_Entities { get; set; }

    }
}
