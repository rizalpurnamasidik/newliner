﻿using Application.GenericApp.Commands.Add;
using Application.GenericApp.Queries.Gets;
using HelloBook_Application.Book.Book.Commands.Add_Book;
using HelloBook_Application.Book.Book.Commands.Change_Book;
using HelloBook_Application.Book.Book.Commands.Remove_Book;
using HelloBook_Application.Book.Book.Queries.Get_Books;
using HelloBook_Application.Book.Book.Queries.Get_BooksWithEncryption;
using HelloBook_Application.Book.Book.Queries.Get_Decryption;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;

namespace HelloBook_Api.Controllers.HelloBook
{
    public class BookController : ApiController
    {

        public BookController()
        {
        }


        //[HttpPost()]
        //public async Task<ActionResult<ResponseBuilder<Add_Book_Response>>> Add([FromForm] Add_Book_Request request)
        //{
        //    return await Mediator.Send(new Add_Command<Add_Book_Request, Add_Book_Response>(request));
        //}

        [HttpPost()]
        public async Task<ActionResult<ResponseBuilder<Add_Book_Response>>> Add([FromBody] Add_Book_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpDelete()]
        public async Task<ActionResult<ResponseBuilder<Remove_Book_Response>>> Remove([FromBody] Remove_Book_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpPut()]
        public async Task<ActionResult<ResponseBuilder<Change_Book_Response>>> Change([FromBody] Change_Book_Command request)
        {
            return await Mediator.Send(request);
        }

        //[HttpGet("/Books")]
        //public async Task<ActionResult<ResponseBuilder<List<Get_Books_Response>>>> Gets()
        //{
        //    return await Mediator.Send(new Gets_Query<Get_Books_Request, Get_Books_Response>());
        //}

        [HttpGet("/Books")]
        public async Task<ActionResult<ResponseBuilder<List<Get_Books_Response>>>> Gets([FromQuery] Get_Books_Query request)
        {
            return await Mediator.Send(request);
        }

        [HttpGet("/BooksWithEncryption")]
        public async Task<ActionResult<ResponseBuilder<List<Get_BooksWithEncryption_Response>>>> Gets([FromQuery] Get_BooksWithEncryption_Query request)
        {
            return await Mediator.Send(request);
        }

        [HttpGet("/Decryption")]
        public async Task<ActionResult<ResponseBuilder<Get_Decryption_Response>>> Gets([FromQuery] Get_Decryption_Query request)
        {
            return await Mediator.Send(request);
        }

    }
}
