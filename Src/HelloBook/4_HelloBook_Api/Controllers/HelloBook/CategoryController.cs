﻿using Microsoft.AspNetCore.Mvc;
using Shared.Responses;
using Application.GenericApp.Queries.Gets;
using Application.GenericApp.Commands.Add;
using Application.GenericApp.Commands.Remove;
using Application.GenericApp.Commands.Change;
using HelloBook_Application.Book.Category.Queries.Get_Categories;
using HelloBook_Application.Book.Category.Commands.Remove_Category;
using HelloBook_Application.Book.Category.Commands.Add_Category;

namespace HelloBook_Api.Controllers.HelloBook
{
    public class CategoryController : ApiController
    {

        public CategoryController()
        {
        }


        [HttpPost()]
        public async Task<ActionResult<ResponseBuilder<Add_Category_Response>>> Add([FromBody] Add_Category_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpDelete()]
        public async Task<ActionResult<ResponseBuilder<Remove_Category_Response>>> Remove([FromBody] Remove_Category_Command request)
        {
            return await Mediator.Send(request);
        }

        //[HttpPut()]
        //public async Task<ActionResult<ResponseBuilder<Change_Category_Response>>> Change([FromBody] Change_Category_Command request)
        //{
        //    return await Mediator.Send(request);
        //}


        [HttpGet("/Categories")]
        public async Task<ActionResult<ResponseBuilder<List<Get_Categories_Response>>>> Gets([FromQuery] Get_Categories_Query request)
        {
            return await Mediator.Send(request);
        }



    }
}
