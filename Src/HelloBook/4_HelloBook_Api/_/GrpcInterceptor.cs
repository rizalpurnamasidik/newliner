﻿using FluentValidation;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Shared.Constants;
using Shared.Exceptions;
using Shared.Extensions;
using Shared.Responses;

namespace HelloBook_Api._
{
    public class GrpcInterceptor : Interceptor
    {
        private readonly IWebHostEnvironment _env;

        public GrpcInterceptor(IWebHostEnvironment env)
        {
            _env = env;
        }

        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
            TRequest request,
            ServerCallContext context,
            UnaryServerMethod<TRequest, TResponse> continuation)
        {
            try
            {
                return await continuation(request, context);
            }
            catch (Exception ex)
            {
                // Note: The gRPC framework also logs exceptions thrown by handlers to .NET Core logging.
                var result = OnException(ex);
                var message = result.ToJsonString();
                var status = new Status(StatusCode.Internal, message);
                throw new RpcException(status);
            }
        }

        private ResponseBuilder<List<string>> OnException(Exception ex)
        {

            var type = ex.GetType();

            // ValidationException fluent valiadtion
            if (type == typeof(ValidationException))
            {
                var exception = ex as ValidationException;

                var validationError = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorType = Error_Type.Validation.ToDescription(),
                        ErrorMessages = exception.Errors.Select(s => s.ErrorMessage).ToList()
                    }
                };

                //context.Result = new BadRequestObjectResult(validationError);

                //context.ExceptionHandled = true;
                return validationError;
            }

            // BadRequestException
            if (type == typeof(BadRequest_Exception))
            {
                var exception = ex as BadRequest_Exception;

                var badRequestException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorType = Error_Type.BadRequest.ToDescription(),
                        ErrorMessages = exception.ErrorsMessage.ToList(),
                    }
                };

                //context.Result = new BadRequestObjectResult(badRequestException);

                //context.ExceptionHandled = true;
                return badRequestException;
            }

            // UnauthorizedAccessException
            if (type == typeof(UnauthorizedAccessException))
            {
                var exception = ex as UnauthorizedAccessException;

                var unauthorizedAccessException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorType = Error_Type.UnauthorizedAccess.ToDescription(),
                        ErrorMessages = new List<string> { exception.Message },
                    }
                };

                if (_env.EnvironmentName.ToLower().Contains("dev"))
                {
                    unauthorizedAccessException.Error.ErrorMessages.Add(ex.StackTrace);
                }


                //context.Result = new UnauthorizedObjectResult(unauthorizedAccessException);

                //context.ExceptionHandled = true;
                return unauthorizedAccessException;
            }

            // UnauthenticatedException
            if (type == typeof(Unauthenticated_Exception))
            {
                var exception = ex as Unauthenticated_Exception;

                var unauthorizedAccessException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorType = Error_Type.Unauthenticated.ToDescription(),
                        ErrorMessages = exception.ErrorsMessage.ToList(),
                    }
                };

                if (_env.EnvironmentName.ToLower().Contains("dev"))
                {
                    unauthorizedAccessException.Error.ErrorMessages.Add(ex.StackTrace);
                }


                //context.Result = new UnauthorizedObjectResult(unauthorizedAccessException);

                //context.ExceptionHandled = true;
                return unauthorizedAccessException;
            }


            // untuk error yang belum di handle
            var unknownError = new ResponseBuilder<List<string>>
            {
                Error = new Error
                {
                    IsError = true,
                    ErrorType = Error_Type.Unknown.ToDescription(),
                    ErrorMessages = new List<string> { "Internal Server Error" }
                }
            };

            if (_env.EnvironmentName.ToLower().Contains("dev"))
            {
                unknownError.Error.ErrorMessages.Add(ex.Message);
                if (ex.Message.Contains("inner exception"))
                {
                    unknownError.Error.ErrorMessages.Add(ex.InnerException.Message);
                }
                unknownError.Error.ErrorMessages.Add(ex.StackTrace);
            }

            //context.Result = new ObjectResult(unknownError)
            //{
            //    StatusCode = StatusCodes.Status500InternalServerError
            //};
            //context.ExceptionHandled = true;
            return unknownError;

        }

    }
}
