using System.Reflection;
using System.Text;
using HelloBook_Api._;
using HelloBook_Api.Grpcs;
using HelloBook_Application;
using HelloBook_Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.IdentityModel.Tokens;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Host.UseSerilog();
var services = builder.Services;
var configuration = builder.Configuration;


// /// //// /////
services.AddControllers(
    options =>
    {
        //options.ModelValidatorProviders.Clear();
        options.Filters.Add<ExceptionFilter>();
        //options.Filters.Add<ValidationFilter>();
        options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;
    });
services.AddGrpc(options =>
{
    {
        options.Interceptors.Add<GrpcInterceptor>();
        options.EnableDetailedErrors = true;
    }
});
services.AddGrpcReflection();



services.AddRazorPages()
    .AddViewOptions(options =>
    {
        options.HtmlHelperOptions.ClientValidationEnabled = false;
    });
services.AddHelloBookApplication();
services.AddHelloBookInfrastructure(configuration);
services.AddSwagger();

var app = builder.Build();


app.MapGrpcService<Book_Grpc>();
var env = app.Environment;
if (env.IsDevelopment()) { app.MapGrpcReflectionService(); }

var option = new RewriteOptions();
option.AddRedirect("^$", "swagger");
app.UseRewriter(option);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");


// /// //// /////
app.UseSwaggerConfigure();

app.Run();
