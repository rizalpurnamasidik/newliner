﻿

using Grpc.Core;
using HelloBook_Api._.Protos;
using HelloBook_Application.Book.Book.Commands.Add_Book;
using HelloBook_Application.Book.Book.Commands.Remove_Book;
using HelloBook_Application.Book.Book.Queries.Get_Books;
using HelloBook_Application.Book.Book.Queries.Get_CatalogBooks;
using MediatR;

namespace HelloBook_Api.Grpcs
{
    public class Book_Grpc : BookProto.BookProtoBase
    {
        private readonly IMediator _mediator;
        private readonly IHttpContextAccessor _httpContext;

        public Book_Grpc(IMediator mediator, IHttpContextAccessor httpContext)
        {
            _mediator = mediator;
            _httpContext = httpContext;
        }

        public override async Task<_.Protos.Get_Books_Response> Get_Books(_.Protos.Get_Books_Request request, ServerCallContext context)
        {
            //
            // get data
            var data = await _mediator.Send(new Get_Books_Query
            {
            });

            //
            // build list data
            var dataItems = new List<Get_Books_Response_item>();
            foreach (var item in data.Data)
            {
                var dataItem = new Get_Books_Response_item
                {
                    Id = item.Id.ToString(),
                    Author = item.Author,
                    CategoryId = item.CategoryId.HasValue ? item.CategoryId.Value.ToString() : "",
                    Title = item.Title,
                    UnitPrice = item.UnitPrice
                };

                dataItems.Add(dataItem);
            }


            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(data.Error.ErrorMessages);
            error.IsError = data.Error.IsError;
            error.ErrorType = data.Error.ErrorType ?? "";

            //var response = new _.Protos.Get_Books_Response
            //{
            //    Message = data.Message,
            //    Error = error
            //};
            var response = new _.Protos.Get_Books_Response
            {
                Message = data.Message ?? "",
                Error = error
            };
            response.Data.Add(dataItems);

            return response;

        }


        public override async Task<_.Protos.Add_Book_Response> Add_Book(_.Protos.Add_Book_Request request, ServerCallContext context)
        {
            //
            // save data
            var addResult = await _mediator.Send(new Add_Book_Command
            {
                Author = request.Author,
                Title = request.Title,
                UnitPrice = request.UnitPrice,
            });


            //
            // build data
            var dataItem = new Add_Book_Response_Item
            {
                Id = addResult.Data.Id.ToString()
            };

            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(addResult.Error.ErrorMessages);
            error.IsError = addResult.Error.IsError;
            error.ErrorType = addResult.Error.ErrorType ?? "";

            //
            // build response
            var response = new _.Protos.Add_Book_Response
            {
                Message = addResult.Message ?? "",
                Error = error,
                Data = dataItem
            };

            return response;
        }

        public override async Task<_.Protos.Remove_Book_Response> Remove_Book(_.Protos.Remove_Book_Request request, ServerCallContext context)
        {
            //
            // remove data
            var removeResult = await _mediator.Send(new Remove_Book_Command
            {
                Id = Guid.Parse(request.Id),
            });

            //
            // build data
            var dataItem = new Remove_Book_Response_Item
            {
                Id = removeResult.Data.Id.ToString()
            };

            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(removeResult.Error.ErrorMessages);
            error.IsError = removeResult.Error.IsError;
            error.ErrorType = removeResult.Error.ErrorType ?? "";

            //
            // build response
            var response = new _.Protos.Remove_Book_Response
            {
                Message = removeResult.Message ?? "",
                Error = error,
                Data = dataItem
            };

            return response;
        }

        public override async Task<_.Protos.Get_CatalogBooks_Response> Get_CatalogBooks(_.Protos.Get_CatalogBooks_Request request, ServerCallContext context)
        {
            //
            // get data
            var data = await _mediator.Send(new Get_CatalogBooks_Query
            {
            });

            //
            // build list data
            var dataItems = new List<Get_CatalogBooks_Response_item>();
            foreach (var item in data.Data)
            {
                var dataItem = new Get_CatalogBooks_Response_item
                {
                    Id = item.Id.ToString(),
                    Author = item.Author,
                    CategoryId = item.CategoryId.HasValue ? item.CategoryId.Value.ToString() : "",
                    Title = item.Title,
                    UnitPrice = item.UnitPrice
                };

                dataItems.Add(dataItem);
            }


            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(data.Error.ErrorMessages);
            error.IsError = data.Error.IsError;
            error.ErrorType = data.Error.ErrorType ?? "";

            //
            // response
            var response = new _.Protos.Get_CatalogBooks_Response
            {
                Message = data.Message ?? "",
                Error = error
            };
            response.Data.Add(dataItems);

            return response;
        }
    }
}
