﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HelloBook_Infrastructure.Databases.HelloBook.Migrations
{
    public partial class UnitPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UnitPrice",
                schema: "Book",
                table: "Book",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UnitPrice",
                schema: "Book",
                table: "Book");
        }
    }
}
