﻿using Application.Interfaces;
using HelloBook_Application._.Interfaces;
using HelloBook_Domain.HelloBook;
using HelloBook_Infrastructure.Settings;
using Infrastructure.DataSources;
using Infrastructure.Extension;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace HelloBook_Infrastructure.DataSources.HelloBook
{

    public class HelloBook_DbContext : DatabaseContext, IHelloBook_DbContext
    {

        public DbContext Instance => this;

        public HelloBook_DbContext(DbContextOptions<HelloBook_DbContext> options,
            IOptions<HelloBookSettings> config
            , IUserLogin userLogin
            ) : base(options, config, userLogin)
        {
            DapperInit(nameof(HelloBook_DbContext));
        }

        public DbSet<HelloBook_Book_Entity> HelloBook_Book_Entities { get; set; }
        public DbSet<HelloBook_Category_Entity> HelloBook_Category_Entities { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.ApplyAllConfigurationsFromNameSpace("Todo_Infrastructure.Databases.Todo.Configs");


            builder.Entity<HelloBook_Book_Entity>().Property(b => b.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Entity<HelloBook_Category_Entity>().Property(b => b.Id).HasDefaultValueSql("uuid_generate_v4()");
        }
    }
}
