﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure;
using Infrastructure.Extension;
using HelloBook_Infrastructure.Settings;
using HelloBook_Application._.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using HelloBook_Infrastructure.DataSources.HelloBook;

namespace HelloBook_Infrastructure
{
    public static class Startup
    {
        public static IServiceCollection AddHelloBookInfrastructure(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddInfrastructure();
            services.LoadSettings<HelloBookSettings>(configuration);

            services.AddDbContext(configuration, db =>
            {
                db.DbConnect<IHelloBook_DbContext, HelloBook_DbContext>(configuration);
            });



            // /// //// /////
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                            .GetBytes("BVcdrftgyhjvgcfxdrTFYGUHIRXfcgvh76eudgHUI*ytFGturIGhj^&%^#4758769youGHGJDy7850769*PYOULGKHJGHJFsrer6")),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                });


            return services;
        }
    }
}
