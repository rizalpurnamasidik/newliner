﻿using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities;
using Domain.Interfaces;

namespace HelloBook_Domain.HelloBook
{
    [Table("Book", Schema = "Book")]
    public class HelloBook_Book_Entity : Auditable_Entity<Guid>, IAuditTrail_Entity
    {
        public Guid? CategoryId { get; set; }
        public string? Title { get; set; } = string.Empty;
        public string? Author { get; set; } = string.Empty;
        public int UnitPrice { get; set; }

    }
}
