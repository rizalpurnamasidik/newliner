﻿using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities;

namespace HelloBook_Domain.HelloBook
{
    [Table("Category", Schema = "Book")]
    public class HelloBook_Category_Entity : Auditable_Entity<Guid>
    {
        public string CategoryName { get; set; } = string.Empty;

    }
}
