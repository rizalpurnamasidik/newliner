﻿using Grpc.Net.Client;
using HelloBook_Api._.Protos;
using Microsoft.Extensions.Options;

namespace Frontend.Endpoints
{
    public class Book_Endpoint
    {
        private readonly Appsettings _appsettings;
        private readonly BookProto.BookProtoClient _bookClient;

        public Book_Endpoint(
            IOptions<Appsettings> appsettings
            , BookProto.BookProtoClient bookClient
            )
        {
            _appsettings = appsettings.Value;
            _bookClient = bookClient;
        }


        public async Task<Get_Books_Response> Get_Books()
        {

            //var httpHandler = new HttpClientHandler
            //{
            //    ServerCertificateCustomValidationCallback =
            //    HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            //};

            //using var channel = GrpcChannel.ForAddress(_appsettings.Endpoints.BookEndpoint,
            //   new GrpcChannelOptions
            //   {
            //       HttpHandler = httpHandler,
            //       MaxReceiveMessageSize = null,
            //   });

            //var client = new BookProto.BookProtoClient(channel);

            //var token = new CancellationTokenSource(TimeSpan.FromSeconds(60));
            //var result = await client.Get_BooksAsync(
            //    new Get_Books_Request { Length = 10, Page = 1 },
            //    cancellationToken: token.Token
            //);

            //return result;

            var result = await _bookClient.Get_BooksAsync(new Get_Books_Request { Length = 10, Page = 1 });
            return result;
        }

        public async Task<Add_Book_Response> Add_Book(string author, string title)
        {
            var result = await _bookClient.Add_BookAsync(new Add_Book_Request
            {
                Author = author,
                Title = title,
            });
            return result;
        }

        public async Task<Remove_Book_Response> Remove_Book(string id)
        {
            var result = await _bookClient.Remove_BookAsync(new Remove_Book_Request
            {
                Id = id
            });
            return result;
        }
    }
}
