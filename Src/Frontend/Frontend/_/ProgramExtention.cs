﻿using Basket_Api._.Protos;
using Frontend._.GrpcInterceptors;
using Frontend.Endpoints;
using HelloBook_Api._.Protos;
using Identity_Api._.Protos;

namespace Frontend._
{
    public static class ProgramExtention
    {
        public static IServiceCollection AddProgramExtention(this IServiceCollection services, ConfigurationManager configuration)
        {

            services.AddHttpContextAccessor();
            services.AddSingleton<UserLogin>();

            services.AddGrpcService(configuration);
            return services;
        }

        private static IServiceCollection AddGrpcService(this IServiceCollection services, ConfigurationManager configuration)
        {

            // book
            services.AddTransient<Book_Endpoint>();
            services.AddSingleton<Book_GrpcInterceptor>();
            services.AddGrpcClient<BookProto.BookProtoClient>(o =>
            {
                var appsettings = configuration.Get<Appsettings>();
                o.Address = new Uri(appsettings.Endpoints.BookEndpoint);
            }).ConfigurePrimaryHttpMessageHandler(() =>
            {
                var handler = new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                };
                return handler;
            }).AddInterceptor<Book_GrpcInterceptor>();


            // basket
            services.AddSingleton<Basket_GrpcInterceptor>();
            services.AddGrpcClient<BasketProto.BasketProtoClient>(o =>
            {
                var appsettings = configuration.Get<Appsettings>();
                o.Address = new Uri(appsettings.Endpoints.BasketEndpoint);
            }).ConfigurePrimaryHttpMessageHandler(() =>
            {
                var handler = new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                };
                return handler;
            }).AddInterceptor<Basket_GrpcInterceptor>();

            // identity
            services.AddSingleton<Identity_GrpcInterceptor>();
            services.AddGrpcClient<IdentityProto.IdentityProtoClient>(o =>
            {
                var appsettings = configuration.Get<Appsettings>();
                o.Address = new Uri(appsettings.Endpoints.IdentityEndpoint);
            }).ConfigurePrimaryHttpMessageHandler(() =>
            {
                var handler = new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                };
                return handler;
            }).AddInterceptor<Identity_GrpcInterceptor>();


            return services;

        }

    }
}
