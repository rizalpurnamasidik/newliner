﻿namespace Frontend._
{
    public class UserLogin
    {
        private readonly IHttpContextAccessor _httpContext;

        public UserLogin(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        public void ClearAll()
        {
            _httpContext.HttpContext.Response.Cookies.Delete("Username");
            _httpContext.HttpContext.Response.Cookies.Delete("Email");
            _httpContext.HttpContext.Response.Cookies.Delete("JwtToken");
            _httpContext.HttpContext.Response.Cookies.Delete("RefreshToken");
        }

        public bool IsAuthenticated
        {
            get
            {
                if (string.IsNullOrEmpty(JwtToken))
                {
                    return false;
                }
                return true;
            }
        }


        public string? JwtToken
        {
            get
            {
                var token = _httpContext.HttpContext.Request.Cookies["JwtToken"];

                if (token == null)
                { return ""; }
                return token;
            }

            set
            {

                if (value != null)
                {
                    // set token to cookies
                    var cookieOptions = new CookieOptions
                    {
                        Expires = DateTime.Now.AddHours(1),
                        HttpOnly = true,
                    };
                    _httpContext.HttpContext.Response.Cookies.Append("JwtToken", value, cookieOptions);
                }
            }

        }

        public string? RefreshToken
        {
            get
            {
                var token = _httpContext.HttpContext.Request.Cookies["RefreshToken"];
                if (token == null)
                { return ""; }
                return token;
            }

            set
            {

                if (value != null)
                {
                    // set token to cookies
                    var cookieOptions = new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(6),
                        HttpOnly = true,
                    };
                    _httpContext.HttpContext.Response.Cookies.Append("RefreshToken", value, cookieOptions);
                }
            }

        }

        public string? Email
        {
            get
            {
                var token = _httpContext.HttpContext.Request.Cookies["Email"];
                return token;
            }

            set
            {

                if (value != null)
                {
                    // set token to cookies
                    var cookieOptions = new CookieOptions
                    {
                        Expires = DateTime.Now.AddYears(10),
                        HttpOnly = true,
                    };
                    _httpContext.HttpContext.Response.Cookies.Append("Email", value, cookieOptions);
                }
            }

        }

        public string? Username
        {
            get
            {
                var token = _httpContext.HttpContext.Request.Cookies["Username"];
                return token;
            }

            set
            {

                if (value != null)
                {
                    // set token to cookies
                    var cookieOptions = new CookieOptions
                    {
                        Expires = DateTime.Now.AddYears(10),
                        HttpOnly = true,
                    };
                    _httpContext.HttpContext.Response.Cookies.Append("Username", value, cookieOptions);
                }
            }

        }


    }
}
