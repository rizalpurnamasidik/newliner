﻿using Grpc.Core;
using Grpc.Core.Interceptors;
using HelloBook_Api._.Protos;
using Microsoft.Extensions.Options;

namespace Frontend._.GrpcInterceptors
{
    public class Basket_GrpcInterceptor : Interceptor
    {
        private readonly BookProto.BookProtoClient _bookClient;
        private readonly Appsettings _appsettings;
        private readonly UserLogin _userLogin;

        public Basket_GrpcInterceptor(
            //BookProto.BookProtoClient bookClient
            IOptions<Appsettings> appsettings
            , UserLogin userLogin
            )
        {
            //_bookClient = bookClient;
            _appsettings = appsettings.Value;
            _userLogin = userLogin;
        }


        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
            TRequest request,
            ServerCallContext context,
            UnaryServerMethod<TRequest, TResponse> continuation)
        {


            return await continuation(request, context);
        }


        public override AsyncUnaryCall<TResponse> AsyncUnaryCall<TRequest, TResponse>(TRequest request, ClientInterceptorContext<TRequest, TResponse> context, AsyncUnaryCallContinuation<TRequest, TResponse> continuation)
        {
            var headers = new Metadata
            {
                new Metadata.Entry("Authorization", $"bearer {_userLogin.JwtToken}")
            };

            var newOptions = context.Options.WithHeaders(headers);

            var newContext = new ClientInterceptorContext<TRequest, TResponse>(
                context.Method,
                context.Host,
                newOptions);

            return base.AsyncUnaryCall(request, newContext, continuation);
        }


    }
}
