﻿using Grpc.Core;
using Grpc.Core.Interceptors;
using HelloBook_Api._.Protos;
using Microsoft.Extensions.Options;

namespace Frontend._.GrpcInterceptors
{
    public class Identity_GrpcInterceptor : Interceptor
    {
        private readonly Appsettings _appsettings;

        public Identity_GrpcInterceptor(
            IOptions<Appsettings> appsettings
            )
        {
            _appsettings = appsettings.Value;
        }


        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
            TRequest request,
            ServerCallContext context,
            UnaryServerMethod<TRequest, TResponse> continuation)
        {


            return await continuation(request, context);
        }


        public override AsyncUnaryCall<TResponse> AsyncUnaryCall<TRequest, TResponse>(TRequest request, ClientInterceptorContext<TRequest, TResponse> context, AsyncUnaryCallContinuation<TRequest, TResponse> continuation)
        {
            var headers = new Metadata
            {
                new Metadata.Entry("Authorization", $"bearer _userLogin.JwtToken")
            };

            var newOptions = context.Options.WithHeaders(headers);

            var newContext = new ClientInterceptorContext<TRequest, TResponse>(
                context.Method,
                context.Host,
                newOptions);

            return base.AsyncUnaryCall(request, newContext, continuation);
        }


    }
}
