﻿using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Shared.Constants;
using Shared.Extensions;
using Shared.Responses;

namespace Frontend._.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly IWebHostEnvironment _env;

        public ExceptionFilter(IWebHostEnvironment env)
        {
            _env = env;
        }

        private class RpcResponseBuilder
        {
            public ErrorResponse Error { get; set; } = new ErrorResponse();
            public string Message { get; set; }

        }

        private class ErrorResponse
        {
            public bool IsError { get; set; } = false;
            public string ErrorType { get; set; }
            public string ErrorId { get; set; }
            public List<string> ErrorMessages { get; set; } = new List<string>();
        }

        public override void OnException(ExceptionContext context)
        {

            var type = context.Exception.GetType();
            var controller = context.RouteData.Values["controller"].ToString();
            var action = context.RouteData.Values["action"].ToString();

            if (type == typeof(RpcException))
            {
                var exception = context.Exception as RpcException;
                var errorMessages = new List<string>();
                if (exception?.Status.Detail.IsValidJson() == true)
                {
                    errorMessages = exception.Status.Detail.ToJsonObject<RpcResponseBuilder>(removeAllComment: true).Error.ErrorMessages;
                }
                else
                {
                    errorMessages.Add(exception?.Status.Detail);
                }


                var validationError = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorType = Error_Type.Unknown.ToDescription(),
                        ErrorMessages = errorMessages
                    }
                };


                context.Result = new BadRequestObjectResult(validationError);

                context.ExceptionHandled = true;
                return;
            }

            //// ValidationException fluent valiadtion
            //if (type == typeof(ValidationException))
            //{
            //    var exception = context.Exception as ValidationException;

            //    var validationError = new ResponseBuilder<List<string>>
            //    {
            //        Error = new Error
            //        {
            //            IsError = true,
            //            Type = ErrorType.Validation.ToDescription(),
            //            Messages = exception.Errors.Select(s => s.ErrorMessage).ToList()
            //        }
            //    };

            //    context.Result = new BadRequestObjectResult(validationError);

            //    context.ExceptionHandled = true;
            //    return;
            //}

            //// BadRequestException
            //if (type == typeof(BadRequestException))
            //{
            //    var exception = context.Exception as BadRequestException;

            //    var badRequestException = new ResponseBuilder<List<string>>
            //    {
            //        Error = new Error
            //        {
            //            IsError = true,
            //            Type = ErrorType.BadRequest.ToDescription(),
            //            Messages = exception.ErrorsMessage.ToList(),
            //        }
            //    };

            //    context.Result = new BadRequestObjectResult(badRequestException);

            //    context.ExceptionHandled = true;
            //    return;
            //}

            //// UnauthorizedAccessException
            //if (type == typeof(UnauthorizedAccessException))
            //{
            //    var exception = context.Exception as UnauthorizedAccessException;

            //    var unauthorizedAccessException = new ResponseBuilder<List<string>>
            //    {
            //        Error = new Error
            //        {
            //            IsError = true,
            //            Type = ErrorType.UnauthorizedAccess.ToDescription(),
            //            Messages = new List<string> { exception.Message },
            //        }
            //    };

            //    if (_env.EnvironmentName.ToLower().Contains("dev"))
            //    {
            //        unauthorizedAccessException.Error.Messages.Add(context.Exception.StackTrace);
            //    }


            //    context.Result = new UnauthorizedObjectResult(unauthorizedAccessException);

            //    context.ExceptionHandled = true;
            //    return;
            //}

            //// UnauthenticatedException
            //if (type == typeof(UnauthenticatedException))
            //{
            //    var exception = context.Exception as UnauthenticatedException;

            //    var unauthorizedAccessException = new ResponseBuilder<List<string>>
            //    {
            //        Error = new Error
            //        {
            //            IsError = true,
            //            Type = ErrorType.Unauthenticated.ToDescription(),
            //            Messages = exception.ErrorsMessage.ToList(),
            //        }
            //    };

            //    if (_env.EnvironmentName.ToLower().Contains("dev"))
            //    {
            //        unauthorizedAccessException.Error.Messages.Add(context.Exception.StackTrace);
            //    }


            //    context.Result = new UnauthorizedObjectResult(unauthorizedAccessException);

            //    context.ExceptionHandled = true;
            //    return;
            //}


            // untuk error yang belum di handle
            var unknownError = new ResponseBuilder<List<string>>
            {
                Error = new Error
                {
                    IsError = true,
                    ErrorType = Error_Type.Unknown.ToDescription(),
                    ErrorMessages = new List<string> { "Internal Server Error" }
                }
            };

            if (_env.EnvironmentName.ToLower().Contains("dev"))
            {
                unknownError.Error.ErrorMessages.Add(context.Exception.Message);
                if (context.Exception.Message.Contains("inner exception"))
                {
                    unknownError.Error.ErrorMessages.Add(context.Exception.InnerException.Message);
                }
                unknownError.Error.ErrorMessages.Add(context.Exception.StackTrace);
            }

            context.Result = new ObjectResult(unknownError)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            context.ExceptionHandled = true;


        }

    }
}
