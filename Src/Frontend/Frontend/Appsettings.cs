﻿namespace Frontend
{
    public class Appsettings
    {
        public Endpoint Endpoints { get; set; } = new();

    }

    public class Endpoint
    {
        public string BookEndpoint { get; set; }
        public string BasketEndpoint { get; set; }
        public string IdentityEndpoint { get; set; }
    }
}
