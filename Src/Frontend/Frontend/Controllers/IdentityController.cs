﻿using Frontend._;
using Frontend.Endpoints;
using Frontend.Models;
using HelloBook_Api._.Protos;
using Identity_Api._.Protos;
using Microsoft.AspNetCore.Mvc;
using Shared.Extensions;
using System.Diagnostics;

namespace Frontend.Controllers
{
    [Route("[controller]")]
    public class IdentityController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IdentityProto.IdentityProtoClient _identityProtoClient;
        private readonly UserLogin _userLogin;

        public IdentityController(ILogger<HomeController> logger
            , IdentityProto.IdentityProtoClient identityProtoClient
            , UserLogin userLogin
            )
        {
            _logger = logger;
            _identityProtoClient = identityProtoClient;
            _userLogin = userLogin;
        }

        [HttpGet("Login")]
        public async Task<IActionResult> Login()
        {
            return View();
        }

        [HttpPost("Login")]
        public async Task<IActionResult> LoginPost([FromForm] string email, [FromForm] string password)
        {
            var result = await _identityProtoClient.Login_WithEmailAsync(new Login_WithEmail_Request
            {
                Email = email,
                Password = password
            });

            // set cookies
            _userLogin.JwtToken = result.Data.JwtToken;
            _userLogin.Email = result.Data.Email;

            return RedirectToAction("Get_CatalogBooks", "Catalog");
        }


        [HttpGet("Register")]
        public async Task<IActionResult> Register()
        {
            return View();
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterPost([FromForm] string email, [FromForm] string password)
        {
            var result = await _identityProtoClient.Register_WithEmailAsync(new Register_WithEmail_Request
            {
                Email = email,
                Password = password
            });

            return RedirectToAction("Login", "Identity");
        }


        [HttpGet("Logout")]
        public IActionResult Logout()
        {
            // clear all cookies
            _userLogin.ClearAll();
            return RedirectToAction("Get_CatalogBooks", "Catalog");
        }

    }


}