﻿using Frontend.Endpoints;
using Frontend.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Frontend.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Book_Endpoint _bookEndpoint;

        public HomeController(ILogger<HomeController> logger
            , Book_Endpoint bookEndpoint
            )
        {
            _logger = logger;
            _bookEndpoint = bookEndpoint;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpGet("/test")]
        public async Task<IActionResult> Test()
        {
            //var feeds = await Mediator.Send(new BrowseFeedQuery { Length = 88, Page = 1 });

            var data = await _bookEndpoint.Get_Books();

            //Response.Cookies.Append("LatestBrowseFeedDate", data.LatestDate.ToString().Replace("\"", ""), new CookieOptions
            //{
            //    Expires = DateTime.Now.AddMonths(7),
            //    HttpOnly = true,
            //});
            //ViewBag.feeds = data.Data.Data.ToList();


            return View();
        }
    }
}