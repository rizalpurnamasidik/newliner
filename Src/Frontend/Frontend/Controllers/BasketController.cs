﻿using Basket_Api._.Protos;
using Frontend.Endpoints;
using Frontend.Models;
using HelloBook_Api._.Protos;
using Microsoft.AspNetCore.Mvc;
using Shared.Extensions;
using System.Diagnostics;

namespace Frontend.Controllers
{
    [Route("[controller]")]
    public class BasketController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BasketProto.BasketProtoClient _basketProto;

        public BasketController(ILogger<HomeController> logger
            , BasketProto.BasketProtoClient basketProto
            )
        {
            _logger = logger;
            _basketProto = basketProto;
        }

        [HttpGet("add-basket")]
        public async Task<IActionResult> Add_Basket(Add_Basket_Request request)
        {
            var result = await _basketProto.Add_BasketAsync(request);
            return RedirectToAction("Get_CatalogBooks", "Catalog");
        }

        [HttpGet("get-baskets")]
        public async Task<IActionResult> Get_Baskets()
        {
            var result = await _basketProto.Get_BasketsAsync(new Get_Baskets_Request { });
            ViewBag.Baskets = result.Data.ToList();
            return View();
        }

        [HttpGet("remove-basket")]
        public async Task<IActionResult> Remove_Basket([FromQuery] Remove_Basket_Request request)
        {
            var result = await _basketProto.Remove_BasketAsync(request);
            return RedirectToAction("Get_Baskets", "Basket");
        }

    }


}