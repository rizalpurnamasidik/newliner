﻿using Frontend.Endpoints;
using Frontend.Models;
using HelloBook_Api._.Protos;
using Microsoft.AspNetCore.Mvc;
using Shared.Extensions;
using System.Diagnostics;

namespace Frontend.Controllers
{
    [Route("[controller]")]
    public class CatalogController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BookProto.BookProtoClient _bookClient;

        public CatalogController(ILogger<HomeController> logger
            , BookProto.BookProtoClient bookClient
            )
        {
            _logger = logger;
            _bookClient = bookClient;
        }

        [HttpGet()]
        public async Task<IActionResult> Get_CatalogBooks()
        {
            var result = await _bookClient.Get_CatalogBooksAsync(new Get_CatalogBooks_Request());
            ViewBag.CatalogBooks = result.Data.ToList();
            return View();
        }

        [HttpGet("get-catalogbooks-api")]
        public async Task<IActionResult> Get_CatalogBooksApi()
        {
            var result = await _bookClient.Get_CatalogBooksAsync(new Get_CatalogBooks_Request());
            return Ok(result);
        }
    }


}