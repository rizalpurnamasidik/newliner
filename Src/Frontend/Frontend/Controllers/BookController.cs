﻿using Frontend.Endpoints;
using Frontend.Models;
using HelloBook_Api._.Protos;
using Microsoft.AspNetCore.Mvc;
using Shared.Extensions;
using System.Diagnostics;

namespace Frontend.Controllers
{
    [Route("[controller]")]
    public class BookController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Book_Endpoint _bookEndpoint;
        private readonly BookProto.BookProtoClient _bookClient;

        public BookController(ILogger<HomeController> logger
            , Book_Endpoint bookEndpoint
            , BookProto.BookProtoClient bookClient
            )
        {
            _logger = logger;
            _bookEndpoint = bookEndpoint;
            _bookClient = bookClient;
        }

        [HttpGet("get-books")]
        public async Task<IActionResult> GetBooks()
        {
            return View();
        }

        [HttpGet("get-books-api")]
        public async Task<IActionResult> GetBooksApi()
        {
            var data = await _bookEndpoint.Get_Books();
            return Ok(data);
        }

        [HttpPost("get-books-api")]
        public async Task<IActionResult> Add_Book([FromForm] string values)
        {
            var valuesObject = values.ToJsonObject<CobaPost>();
            var result = await _bookClient.Add_BookAsync(new Add_Book_Request
            {
                Author = valuesObject.Author,
                Title = valuesObject.Title,
                UnitPrice = valuesObject.UnitPrice
            });
            return Ok(result);
        }

        [HttpDelete("get-books-api")]
        public async Task<IActionResult> Remove_Book([FromForm] string key)
        {
            var result = await _bookClient.Remove_BookAsync(new Remove_Book_Request
            {
                Id = key
            });
            return Ok(result);
        }
    }

    public class CobaPost
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public int UnitPrice { get; set; }
    }
}