﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Application.Interfaces.Cryptography;
using Identity_Application._.Classes;
using Identity_Application._.Interfaces;
using Identity_Infrastructure.Settings;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared.Constants;
using Shared.Extensions;
using System.Security.Cryptography;
using Application.Interfaces.Logs;

namespace Identity_Infrastructure.Identity
{
    public class Identity : IIdentity
    {
        private readonly ICryptography _cryptography;
        private readonly IdentitySettings _identitySettings;
        private readonly IIdentity_DbContext _identityDb;
        private readonly ILog _log;

        public Identity(
             ICryptography cryptography
            , IOptions<IdentitySettings> identitySettings
            , IIdentity_DbContext identityDb
            , ILog log
            )
        {
            _cryptography = cryptography;
            _identitySettings = identitySettings.Value;
            _identityDb = identityDb;
            _log = log;
        }

        public async Task<JwtResponse> Jwt(IdentityUser user)
        {
            _log.LogInformation($"JWT SECRET : {_identitySettings.ToJsonString()}");
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_identitySettings.Jwt.Secret));

            // TODO : config
            var token = new JwtSecurityToken(
                issuer: _identitySettings.Jwt.Issuer,
                audience: "audience",
                expires: DateTime.UtcNow
                    .AddHours(_identitySettings.Jwt.ExpireInHours)
                    .AddMinutes(_identitySettings.Jwt.ExpireInMinutes)
                    .AddSeconds(_identitySettings.Jwt.ExpireInSeconds),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256),
                claims: new List<Claim>
                {
                    new Claim(Claim_Type.Email.ToDescription(), _cryptography.AesEncrypt(user.Email)),
                }
            );

            var tokenAsString = new JwtSecurityTokenHandler().WriteToken(token);

            var jwtToken = new JwtResponse
            {
                Token = tokenAsString,
            };

            // generate refresh token

            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);


            return jwtToken;
        }

    }
}
