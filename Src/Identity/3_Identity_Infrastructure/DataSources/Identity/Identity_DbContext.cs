﻿using Identity_Application._.Interfaces;
using Identity_Infrastructure.Settings;
using Infrastructure.DataSources;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Identity_Infrastructure.DataSources.Identity
{

    public class Identity_DbContext : IdentityDatabaseContext, IIdentity_DbContext
    {

        public DbContext Instance => this;

        public Identity_DbContext(DbContextOptions<Identity_DbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Addd the Postgres Extension for UUID generation
            builder.HasPostgresExtension("uuid-ossp");

            builder.Entity<IdentityUser>(entity => { entity.ToTable(name: "Users", schema: "Identity"); });
            builder.Entity<IdentityRole>(entity => { entity.ToTable(name: "Roles", schema: "Identity"); });
            builder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("UserRoles", schema: "Identity"); });
            builder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("UserClaims", schema: "Identity"); });
            builder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("UserLogins", schema: "Identity"); });
            builder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("RoleClaims", schema: "Identity"); });
            builder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("UserTokens", schema: "Identity"); });
        }
    }
}
