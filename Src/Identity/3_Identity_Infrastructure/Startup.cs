﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure;
using Infrastructure.Extension;
using Identity_Infrastructure.Settings;
using Identity_Application._.Interfaces;
using Identity_Infrastructure.DataSources.Identity;

namespace Identity_Infrastructure
{
    public static class Startup
    {
        public static IServiceCollection AddIdentityInfrastructure(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddInfrastructure();
            services.LoadSettings<IdentitySettings>(configuration);

            services.AddDbContext(configuration, db =>
            {
                db.DbConnect<IIdentity_DbContext, Identity_DbContext>(configuration);
            });

            services.AddScoped<IIdentity, Identity.Identity>();

            return services;
        }
    }
}
