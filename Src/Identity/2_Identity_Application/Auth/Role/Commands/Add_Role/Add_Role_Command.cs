﻿using Application.Extensions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Responses;

namespace Identity_Application.Auth.Role.Commands.Add_Role
{
    //[Authorize()]
    public class Add_Role_Command : Add_Role_Request, IRequest<ResponseBuilder<Add_Role_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_Role_Command, ResponseBuilder<Add_Role_Response>>
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public Handler(
            RoleManager<IdentityRole> roleManager
            )
        {
            _roleManager = roleManager;
        }

        public async Task<ResponseBuilder<Add_Role_Response>> Handle(Add_Role_Command request, CancellationToken cancellationToken)
        {

            // build new role
            var newRole = new IdentityRole(request.RoleName);

            // save new role
            var saveNewRole = await _roleManager.CreateAsync(newRole);


            // return 
            return new Add_Role_Response
            {
                Id = newRole.Id
            }.ResponseCreate();
        }


    }
}
