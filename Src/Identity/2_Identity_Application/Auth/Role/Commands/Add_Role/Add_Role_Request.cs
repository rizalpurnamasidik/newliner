﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.Role.Commands.Add_Role
{
    public class Add_Role_Request : Base_Request
    {
        public string? RoleName { get; set; } = string.Empty;
    }

}
