﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.Role.Commands.Remove_Role
{
    public class Remove_Role_Request : Base_Request
    {
        public string? Id { get; set; } = string.Empty;
    }

}
