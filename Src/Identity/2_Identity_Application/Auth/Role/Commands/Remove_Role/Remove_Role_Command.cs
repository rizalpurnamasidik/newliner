﻿using Application.Extensions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Exceptions;
using Shared.Responses;

namespace Identity_Application.Auth.Role.Commands.Remove_Role
{
    //[Authorize()]
    public class Remove_Role_Command : Remove_Role_Request, IRequest<ResponseBuilder<Remove_Role_Response>>
    {

    }

    public class Handler : IRequestHandler<Remove_Role_Command, ResponseBuilder<Remove_Role_Response>>
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public Handler(
            RoleManager<IdentityRole> roleManager
            )
        {
            _roleManager = roleManager;
        }

        public async Task<ResponseBuilder<Remove_Role_Response>> Handle(Remove_Role_Command request, CancellationToken cancellationToken)
        {

            // find role
            var role = await _roleManager.FindByIdAsync(request.Id);

            // validation
            if (role == null)
            {
                throw new NotFound_Exception();
            }

            // delete
            await _roleManager.DeleteAsync(role);


            // return 
            return new Remove_Role_Response
            {
                Id = role.Id
            }.ResponseRemove();
        }


    }
}
