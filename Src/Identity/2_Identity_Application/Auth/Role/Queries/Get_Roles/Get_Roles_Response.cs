﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Identity_Application.Auth.Role.Queries.Get_Roles
{
    public class Get_Roles_Response : Base_Response<string>
    {
        public string? Id { get; set; }
        public string? RoleName { get; set; } = string.Empty;
    }
}
