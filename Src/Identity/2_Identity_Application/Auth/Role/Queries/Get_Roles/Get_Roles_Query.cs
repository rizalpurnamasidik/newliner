﻿using Application.Extensions;
using Application.Interfaces.Logs;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Responses;

namespace Identity_Application.Auth.Role.Queries.Get_Roles
{
    public class Get_Roles_Query : Get_Roles_Request, IRequest<ResponseBuilder<List<Get_Roles_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_Roles_Query, ResponseBuilder<List<Get_Roles_Response>>>
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public Handler(
            RoleManager<IdentityRole> roleManager
            )
        {
            _roleManager = roleManager;
        }

        public async Task<ResponseBuilder<List<Get_Roles_Response>>> Handle(Get_Roles_Query request, CancellationToken cancellationToken)
        {
            // get data
            var data = _roleManager.Roles
                .Select(s => new Get_Roles_Response
                {
                    Id = s.Id,
                    RoleName = s.Name
                }).ToList();


            //
            // return 
            return data.ResponseRead();
        }


    }
}
