﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Identity_Application.Auth.Auth.Commands.Login_WithEmail
{
    public class Login_WithEmail_Response : Base_Response
    {
        public string? JwtToken { get; set; }
        public string? Email { get; set; }
    }
}
