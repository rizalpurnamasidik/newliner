﻿using Application.Extensions;
using Identity_Application._.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Exceptions;
using Shared.Responses;



namespace Identity_Application.Auth.Auth.Commands.Login_WithEmail
{
    public class Login_WithEmail_Command : Login_WithEmail_Request, IRequest<ResponseBuilder<Login_WithEmail_Response>>
    {

    }

    public class Handler : IRequestHandler<Login_WithEmail_Command, ResponseBuilder<Login_WithEmail_Response>>
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IIdentity _identity;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public Handler(
            UserManager<IdentityUser> userManager
            , IIdentity identity
            , RoleManager<IdentityRole> roleManager
            , SignInManager<IdentityUser> signInManager
            )
        {
            _userManager = userManager;
            _identity = identity;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }

        public async Task<ResponseBuilder<Login_WithEmail_Response>> Handle(Login_WithEmail_Command request, CancellationToken cancellationToken)
        {

            var result = await _signInManager.PasswordSignInAsync(request.Email, request.Password, false, lockoutOnFailure: true);

            if (result.IsLockedOut)
            {
                var content = string.Format("Your account is locked out, to reset your password, please click this link: {forgotPassLink}");
                throw new BadRequest_Exception(content);
                //var forgotPassLink = Url.Action(nameof(ForgotPassword), "Account", new { }, Request.Scheme);
                //var message = new Message(new string[] { userModel.Email }, "Locked out account information", content, null);
                //await _emailSender.SendEmailAsync(message);
                //ModelState.AddModelError("", "The account is locked out");
                //return View();
            }
            if (!result.Succeeded)
            {
                //_logger.LogInformation($@"Invalid login attempt [{request.Email}].");
                throw new BadRequest_Exception("Invalid login attempt");
            }


            // get user
            var user = await _userManager.FindByEmailAsync(request.Email);

            // get roles
            var rolesName = await _userManager.GetRolesAsync(user);
            var rolesId = _roleManager.Roles.Where(w => rolesName.Contains(w.Name))
                .Select(s => s.Id).ToList();

            // create JWT
            var jwtToken = await _identity.Jwt(user);

            // return
            return new Login_WithEmail_Response
            {
                JwtToken = jwtToken.Token,
                Email = request.Email,
            }.ResponseRead();

        }


    }
}
