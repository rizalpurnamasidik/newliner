﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.Auth.Commands.Login_WithEmail
{
    public class Login_WithEmail_Request : Base_Request
    {
        public string? Email { get; set; }
        public string? Password { get; set; }
    }

}
