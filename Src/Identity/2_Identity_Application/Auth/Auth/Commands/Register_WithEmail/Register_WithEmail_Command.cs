﻿using Application.Extensions;
using Application.Interfaces.HttpContext;
using Identity_Application._.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Exceptions;
using Shared.Responses;



namespace Identity_Application.Auth.Auth.Commands.Register_WithEmail
{
    public class Register_WithEmail_Command : Register_WithEmail_Request, IRequest<ResponseBuilder<Register_WithEmail_Response>>
    {

    }

    public class Handler : IRequestHandler<Register_WithEmail_Command, ResponseBuilder<Register_WithEmail_Response>>
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IHttpContext _httpContext;

        public Handler(
            UserManager<IdentityUser> userManager
            //, IEmail email,
            , IHttpContext httpContext
            )
        {
            _userManager = userManager;
            _httpContext = httpContext;
        }

        public async Task<ResponseBuilder<Register_WithEmail_Response>> Handle(Register_WithEmail_Command request, CancellationToken cancellationToken)
        {

            // build user data
            var identityUser = new IdentityUser
            {
                Email = request.Email,
                UserName = request.Email,
            };

            // create user
            var result = await _userManager.CreateAsync(identityUser, request.Password);

            // return error
            if (!result.Succeeded)
            {
                // karena menggunakan email, maka setiap error username rubah ke EMAIL
                throw new BadRequest_Exception(result.Errors.Select(s => s.Description.Replace("Username '", "Email '")));
            }

            // get user
            var user = await _userManager.FindByEmailAsync(request.Email);

            // send email
            //var userId = await _userManager.GetUserIdAsync(user);
            //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            //code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
            //var confirmEmail = @$"{_httpContext.BaseUrl}{Routes.Auth.ConfirmEmail}?userId={userId}&code={code}";

            //_email.Send(
            //    emailContext: "tuwo_noreplay",
            //    to: new List<string> { request.Email },
            //    subject: "REGISTER",
            //    body: confirmEmail
            //);



            return new Register_WithEmail_Response
            {
                Id = user.Id
            }.Response();
            //}.Response(ResponseLang.Response_RegistrationSuccessful);
        }


    }
}
