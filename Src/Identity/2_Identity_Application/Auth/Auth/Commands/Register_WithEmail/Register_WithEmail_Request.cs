﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.Auth.Commands.Register_WithEmail
{
    public class Register_WithEmail_Request : Base_Request
    {
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? PasswordRepeat { get; set; }
    }

}
