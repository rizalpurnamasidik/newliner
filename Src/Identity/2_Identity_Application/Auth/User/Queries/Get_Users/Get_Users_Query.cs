﻿using Application.Extensions;
using Application.Interfaces.Logs;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Responses;

namespace Identity_Application.Auth.User.Queries.Get_Users
{
    public class Get_Users_Query : Get_Users_Request, IRequest<ResponseBuilder<List<Get_Users_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_Users_Query, ResponseBuilder<List<Get_Users_Response>>>
    {
        private readonly UserManager<IdentityUser> _userManager;

        public Handler(
            UserManager<IdentityUser> userManager
            )
        {
            _userManager = userManager;
        }

        public async Task<ResponseBuilder<List<Get_Users_Response>>> Handle(Get_Users_Query request, CancellationToken cancellationToken)
        {
            // get data
            var data = _userManager.Users
                .Select(s => new Get_Users_Response
                {
                    Id = s.Id,
                    Email = s.Email
                }).ToList();

            //
            // return 
            return data.ResponseRead();
        }


    }
}
