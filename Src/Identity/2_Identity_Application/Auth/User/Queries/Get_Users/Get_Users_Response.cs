﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Identity_Application.Auth.User.Queries.Get_Users
{
    public class Get_Users_Response : Base_Response<string>
    {
        public string Email { get; set; }
    }
}
