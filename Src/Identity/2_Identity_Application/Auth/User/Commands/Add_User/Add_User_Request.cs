﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.User.Commands.Add_User
{
    public class Add_User_Request : Base_Request
    {
        public string? Email { get; set; }
        public string? Password { get; set; }
        public IEnumerable<string> Role { get; set; }
    }

}
