﻿using FluentValidation;

namespace Identity_Application.Auth.User.Commands.Add_User
{
    public class Add_User_Validator : AbstractValidator<Add_User_Command>
    {
        public Add_User_Validator()
        {
            RuleFor(r => r.Email).NotEmpty();
            RuleFor(r => r.Password).NotEmpty();
        }
    }
}
