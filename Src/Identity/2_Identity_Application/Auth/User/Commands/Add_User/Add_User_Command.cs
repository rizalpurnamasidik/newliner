﻿using Application.Extensions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Exceptions;
using Shared.Responses;

namespace Identity_Application.Auth.User.Commands.Add_User
{
    //[Authorize()]
    public class Add_User_Command : Add_User_Request, IRequest<ResponseBuilder<Add_User_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_User_Command, ResponseBuilder<Add_User_Response>>
    {
        private readonly UserManager<IdentityUser> _userManager;

        public Handler(
            UserManager<IdentityUser> userManager
            )
        {
            _userManager = userManager;
        }

        public async Task<ResponseBuilder<Add_User_Response>> Handle(Add_User_Command request, CancellationToken cancellationToken)
        {
            // build user data
            var identityUser = new IdentityUser
            {
                Email = request.Email,
                UserName = request.Email,
            };

            // create user
            var result = await _userManager.CreateAsync(identityUser, request.Password);

            if (!result.Succeeded)
            {
                // karena menggunakan email, maka setiap error username rubah ke EMAIL
                throw new BadRequest_Exception(result.Errors.Select(s => s.Description.Replace("Username '", "Email '")));
            }



            // return 
            return new Add_User_Response
            {
                Id = identityUser.Id
            }.ResponseCreate();
        }


    }
}
