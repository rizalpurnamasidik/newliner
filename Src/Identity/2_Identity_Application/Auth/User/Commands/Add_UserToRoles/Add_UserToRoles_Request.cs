﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.User.Commands.Add_UserToRoles
{
    public class Add_UserToRoles_Request : Base_Request
    {
        public string? UserId { get; set; }
        public IEnumerable<string> RoleName { get; set; }
    }

}
