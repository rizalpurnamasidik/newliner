﻿using Application.Extensions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Exceptions;
using Shared.Responses;

namespace Identity_Application.Auth.User.Commands.Add_UserToRoles
{
    //[Authorize()]
    public class Add_UserToRoles_Command : Add_UserToRoles_Request, IRequest<ResponseBuilder<Add_UserToRoles_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_UserToRoles_Command, ResponseBuilder<Add_UserToRoles_Response>>
    {
        private readonly UserManager<IdentityUser> _userManager;

        public Handler(
            UserManager<IdentityUser> userManager
            )
        {
            _userManager = userManager;
        }

        public async Task<ResponseBuilder<Add_UserToRoles_Response>> Handle(Add_UserToRoles_Command request, CancellationToken cancellationToken)
        {

            // find user by ID
            var user = await _userManager.FindByIdAsync(request.UserId);
            if (user == null)
            {
                throw new NotFound_Exception();
            }

            // bersihkan semua role sebelum di tambah
            await _userManager.RemoveFromRolesAsync(user, await _userManager.GetRolesAsync(user));


            // add user to role
            var userToRole = await _userManager.AddToRolesAsync(user, request.RoleName);
            if (!userToRole.Succeeded)
            {
                // karena menggunakan email, maka setiap error username rubah ke EMAIL
                throw new BadRequest_Exception(userToRole.Errors.Select(s => s.Description.Replace("Username '", "Email '")));
            }

            // return 
            return new Add_UserToRoles_Response
            {
                Id = user.Id
            }.ResponseCreate();
        }


    }
}
