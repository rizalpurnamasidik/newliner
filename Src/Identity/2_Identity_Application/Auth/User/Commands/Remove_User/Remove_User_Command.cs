﻿using Application.Extensions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Shared.Exceptions;
using Shared.Responses;

namespace Identity_Application.Auth.User.Commands.Remove_User
{
    //[Authorize()]
    public class Remove_User_Command : Remove_User_Request, IRequest<ResponseBuilder<Remove_User_Response>>
    {

    }

    public class Handler : IRequestHandler<Remove_User_Command, ResponseBuilder<Remove_User_Response>>
    {
        private readonly UserManager<IdentityUser> _userManager;

        public Handler(
            UserManager<IdentityUser> userManager
            )
        {
            _userManager = userManager;
        }

        public async Task<ResponseBuilder<Remove_User_Response>> Handle(Remove_User_Command request, CancellationToken cancellationToken)
        {

            // find role
            var user = await _userManager.FindByIdAsync(request.Id);

            // validation
            if (user == null)
            {
                throw new NotFound_Exception();
            }

            // delete
            await _userManager.DeleteAsync(user);


            // return 
            return new Remove_User_Response
            {
                Id = user.Id
            }.ResponseRemove();

        }


    }
}
