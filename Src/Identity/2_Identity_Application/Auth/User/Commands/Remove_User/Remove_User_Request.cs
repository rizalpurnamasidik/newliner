﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Identity_Application.Auth.User.Commands.Remove_User
{
    public class Remove_User_Request : Base_Request
    {
        public string Id { get; set; }
    }

}
