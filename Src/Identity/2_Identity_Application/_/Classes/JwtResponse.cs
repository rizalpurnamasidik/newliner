﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identity_Application._.Classes
{
    public class JwtResponse
    {
        public string? Token { get; set; }
        public string? RefreshToken { get; set; }
    }
}
