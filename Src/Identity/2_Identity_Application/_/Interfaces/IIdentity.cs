﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Identity_Application._.Classes;
using Microsoft.AspNetCore.Identity;

namespace Identity_Application._.Interfaces
{


    public interface IIdentity
    {
        public Task<JwtResponse> Jwt(IdentityUser user);
    }
}
