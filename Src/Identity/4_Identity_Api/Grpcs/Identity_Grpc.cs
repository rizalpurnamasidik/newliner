﻿using Grpc.Core;
using Identity_Api._.Protos;
using Identity_Application.Auth.Auth.Commands.Login_WithEmail;
using Identity_Application.Auth.Auth.Commands.Register_WithEmail;
using MediatR;

namespace Identity_Api.Grpcs
{
    public class Identity_Grpc : IdentityProto.IdentityProtoBase
    {
        private readonly IMediator _mediator;
        private readonly IHttpContextAccessor _httpContext;

        public Identity_Grpc(IMediator mediator, IHttpContextAccessor httpContext)
        {
            _mediator = mediator;
            _httpContext = httpContext;
        }

        public override async Task<_.Protos.Login_WithEmail_Response> Login_WithEmail(_.Protos.Login_WithEmail_Request request, ServerCallContext context)
        {
            //
            // save data
            var loginResult = await _mediator.Send(new Login_WithEmail_Command
            {
                Email = request.Email,
                Password = request.Password
            });


            //
            // build data
            var dataItem = new Login_WithEmail_Response_Item
            {
                Email = loginResult.Data.Email,
                JwtToken = loginResult.Data.JwtToken,
            };

            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(loginResult.Error.ErrorMessages);
            error.IsError = loginResult.Error.IsError;
            error.ErrorType = loginResult.Error.ErrorType ?? "";

            //
            // build response
            var response = new _.Protos.Login_WithEmail_Response
            {
                Message = loginResult.Message ?? "",
                Error = error,
                Data = dataItem
            };

            return response;
        }

        public override async Task<_.Protos.Register_WithEmail_Response> Register_WithEmail(_.Protos.Register_WithEmail_Request request, ServerCallContext context)
        {
            //
            // send data
            var registerResult = await _mediator.Send(new Register_WithEmail_Command
            {
                Email = request.Email,
                Password = request.Password
            });


            //
            // build data
            var dataItem = new Register_WithEmail_Response_Item
            {
                Id = registerResult.Data.Id
            };

            //
            // error
            var error = new Error();
            error.ErrorMessages.AddRange(registerResult.Error.ErrorMessages);
            error.IsError = registerResult.Error.IsError;
            error.ErrorType = registerResult.Error.ErrorType ?? "";

            //
            // build response
            var response = new _.Protos.Register_WithEmail_Response
            {
                Message = registerResult.Message ?? "",
                Error = error,
                Data = dataItem
            };

            return response;
        }
    }
}
