﻿using Identity_Application.Auth.Auth.Commands.Login_WithEmail;
using Identity_Application.Auth.Auth.Commands.Register_WithEmail;
using Identity_Application.Auth.Role.Commands.Add_Role;
using Identity_Application.Auth.Role.Commands.Remove_Role;
using Identity_Application.Auth.Role.Queries.Get_Roles;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;

namespace Identity_Api.Controllers.Auth
{
    public class RoleController : ApiController
    {

        public RoleController()
        {
        }


        [HttpPost("add")]
        public async Task<ActionResult<ResponseBuilder<Add_Role_Response>>> Add_Role([FromForm] Add_Role_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpDelete("remove")]
        public async Task<ActionResult<ResponseBuilder<Remove_Role_Response>>> Remove_Role([FromForm] Remove_Role_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpGet("gets")]
        public async Task<ActionResult<ResponseBuilder<List<Get_Roles_Response>>>> Get_Roles([FromQuery] Get_Roles_Query request)
        {
            return await Mediator.Send(request);
        }
    }
}
