﻿using Identity_Application.Auth.Auth.Commands.Login_WithEmail;
using Identity_Application.Auth.Auth.Commands.Register_WithEmail;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;

namespace Identity_Api.Controllers.Auth
{
    public class AuthController : ApiController
    {

        public AuthController()
        {
        }

        [HttpPost("login-with-email")]
        public async Task<ActionResult<ResponseBuilder<Login_WithEmail_Response>>> LoginWithEmail([FromForm] Login_WithEmail_Command request)
        {
            return await Mediator.Send(request);


        }

        [HttpPost("register-with-email")]
        public async Task<ActionResult<ResponseBuilder<Register_WithEmail_Response>>> RegisterWithEmail([FromForm] Register_WithEmail_Command request)
        {
            return await Mediator.Send(request);


        }

    }
}
