﻿using Identity_Application.Auth.Auth.Commands.Login_WithEmail;
using Identity_Application.Auth.Auth.Commands.Register_WithEmail;
using Identity_Application.Auth.User.Commands.Add_User;
using Identity_Application.Auth.User.Commands.Add_UserToRoles;
using Identity_Application.Auth.User.Commands.Remove_User;
using Identity_Application.Auth.User.Queries.Get_Users;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;

namespace Identity_Api.Controllers.Auth
{
    public class UserController : ApiController
    {

        public UserController()
        {
        }

        [HttpPost("add")]
        public async Task<ActionResult<ResponseBuilder<Add_User_Response>>> Add_User([FromForm] Add_User_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpPost("add-to-role")]
        public async Task<ActionResult<ResponseBuilder<Add_UserToRoles_Response>>> Add_User([FromForm] Add_UserToRoles_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpDelete("remove")]
        public async Task<ActionResult<ResponseBuilder<Remove_User_Response>>> Remove_User([FromForm] Remove_User_Command request)
        {
            return await Mediator.Send(request);
        }


        [HttpGet("gets")]
        public async Task<ActionResult<ResponseBuilder<List<Get_Users_Response>>>> Get_Users([FromQuery] Get_Users_Query request)
        {
            return await Mediator.Send(request);
        }
    }
}
