﻿using Microsoft.AspNetCore.Mvc.Filters;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;
using Shared.Constants;
using Shared.Exceptions;
using Shared.Extensions;
using Application.Interfaces.HttpContext;
using Application.Interfaces.Logs;

namespace Identity_Api._
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly IWebHostEnvironment _env;
        private readonly IHttpContext _httpContext;
        private readonly ILog _log;

        public ExceptionFilter(
            IWebHostEnvironment env
            , IHttpContext httpContext
            , ILog log)
        {
            _env = env;
            _httpContext = httpContext;
            _log = log;
        }

        public override void OnException(ExceptionContext context)
        {

            var type = context.Exception.GetType();

            // ValidationException fluent valiadtion
            if (type == typeof(ValidationException))
            {
                var exception = context.Exception as ValidationException;

                var validationError = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorId = _log.ErrorId(),
                        ErrorType = Error_Type.Validation.ToDescription(),
                        ErrorMessages = exception.Errors.Select(s => s.ErrorMessage).ToList()
                    }
                };

                context.Result = new BadRequestObjectResult(validationError);

                context.ExceptionHandled = true;
                return;
            }

            // BadRequestException
            if (type == typeof(BadRequest_Exception))
            {
                var exception = context.Exception as BadRequest_Exception;

                var badRequestException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorId = _log.ErrorId(),
                        ErrorType = Error_Type.BadRequest.ToDescription(),
                        ErrorMessages = exception.ErrorsMessage.ToList(),
                    }
                };

                context.Result = new BadRequestObjectResult(badRequestException);

                context.ExceptionHandled = true;
                return;
            }

            // NotFoundException
            if (type == typeof(NotFound_Exception))
            {
                var exception = context.Exception as NotFound_Exception;

                var badRequestException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorId = _log.ErrorId(),
                        ErrorType = Error_Type.BadRequest.ToDescription(),
                        ErrorMessages = exception.ErrorsMessage.ToList(),
                    }
                };

                context.Result = new BadRequestObjectResult(badRequestException);

                context.ExceptionHandled = true;
                return;
            }

            // UnauthorizedAccessException
            if (type == typeof(UnauthorizedAccessException))
            {
                var exception = context.Exception as UnauthorizedAccessException;

                var unauthorizedAccessException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorId = _log.ErrorId(),
                        ErrorType = Error_Type.UnauthorizedAccess.ToDescription(),
                        ErrorMessages = new List<string> { exception.Message },
                    }
                };

                if (_env.EnvironmentName.ToLower().Contains("dev"))
                {
                    unauthorizedAccessException.Error.ErrorMessages.Add(context.Exception.StackTrace);
                }


                context.Result = new UnauthorizedObjectResult(unauthorizedAccessException);

                context.ExceptionHandled = true;
                return;
            }

            // UnauthenticatedException
            if (type == typeof(Unauthenticated_Exception))
            {
                var exception = context.Exception as Unauthenticated_Exception;

                var unauthorizedAccessException = new ResponseBuilder<List<string>>
                {
                    Error = new Error
                    {
                        IsError = true,
                        ErrorId = _log.ErrorId(),
                        ErrorType = Error_Type.Unauthenticated.ToDescription(),
                        ErrorMessages = exception.ErrorsMessage.ToList(),
                    }
                };

                if (_env.EnvironmentName.ToLower().Contains("dev"))
                {
                    unauthorizedAccessException.Error.ErrorMessages.Add(context.Exception.StackTrace);
                }


                context.Result = new UnauthorizedObjectResult(unauthorizedAccessException);

                context.ExceptionHandled = true;
                return;
            }


            // untuk error yang belum di handle
            var unknownError = new ResponseBuilder<List<string>>
            {
                Error = new Error
                {
                    IsError = true,
                    ErrorId = _log.ErrorId(),
                    ErrorType = Error_Type.Unknown.ToDescription(),
                    ErrorMessages = new List<string> { "Internal Server Error" }
                }
            };

            if (_env.EnvironmentName.ToLower().Contains("dev"))
            {
                unknownError.Error.ErrorMessages.Add(context.Exception.Message);
                _log.LogError(context.Exception.Message);

                if (context.Exception.Message.Contains("inner exception"))
                {
                    unknownError.Error.ErrorMessages.Add(context.Exception.InnerException.Message);
                    _log.LogError(context.Exception.InnerException.Message);
                }
                unknownError.Error.ErrorMessages.Add(context.Exception.StackTrace);
                _log.LogError(context.Exception.StackTrace);
            }
            else
            {

                _log.LogError(context.Exception.Message);
                if (context.Exception.Message.Contains("inner exception"))
                {
                    _log.LogError(context.Exception.InnerException.Message);
                }
                _log.LogError(context.Exception.StackTrace);
            }

            context.Result = new ObjectResult(unknownError)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            context.ExceptionHandled = true;


        }

    }

}
