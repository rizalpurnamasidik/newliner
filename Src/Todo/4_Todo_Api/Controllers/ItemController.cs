﻿using Application.GenericApp.Commands.Change;
using Microsoft.AspNetCore.Mvc;
using Shared.Responses;
using Todo_Application;
using Todo_Application.Todo.Item.Commands.Add_Items;
using Todo_Application.Todo.Item.Commands.Change_Item;
using Todo_Application.Todo.Item.Commands.Remove_Item;
using Todo_Application.Todo.Item.Queries.Get_Items;

namespace Todo_Api.Controllers
{
    public class ItemController : ApiController
    {

        public ItemController()
        {
        }


        [HttpPost(Routes.Item.Add)]
        public async Task<ActionResult<ResponseBuilder<Add_Item_Response>>> Add([FromForm] Add_Item_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpDelete(Routes.Item.Remove)]
        public async Task<ActionResult<ResponseBuilder<Remove_Item_Response>>> Remove_Todo_Item([FromForm] Remove_Item_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpPut(Routes.Item.Change)]
        public async Task<ActionResult<ResponseBuilder<Change_Item_Response>>> Change([FromForm] Change_Item_Command request)
        {
            return await Mediator.Send(request);
        }

        [HttpGet(Routes.Item.Gets)]
        public async Task<ActionResult<ResponseBuilder<List<Get_Items_Response>>>> Gets()
        {
            return await Mediator.Send(new Get_Items_Query());
        }


    }
}
