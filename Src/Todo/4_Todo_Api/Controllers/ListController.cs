﻿using Microsoft.AspNetCore.Mvc;
using Shared.Responses;
using Application.GenericApp.Queries.Gets;
using Todo_Application;
using Todo_Application.Todo.List.Queries.Get_Lists;
using Todo_Application.Todo.List.Commands.Add_List;
using Application.GenericApp.Commands.Add;
using Todo_Application.Todo.List.Commands.Remove_List;
using Application.GenericApp.Commands.Remove;
using Todo_Application.Todo.List.Commands.Change_List;
using Application.GenericApp.Commands.Change;

namespace Todo_Api.Controllers
{
    public class ListController : ApiController
    {

        public ListController()
        {
        }


        [HttpPost(Routes.List.Add)]
        public async Task<ActionResult<ResponseBuilder<Add_List_Response>>> Add([FromForm] Add_List_Request request)
        {
            return await Mediator.Send(new Add_Command<Add_List_Request, Add_List_Response>(request));
        }

        [HttpDelete(Routes.List.Remove)]
        public async Task<ActionResult<ResponseBuilder<Remove_List_Response>>> Remove([FromForm] Remove_List_Request request)
        {
            return await Mediator.Send(new Remove_Command<Remove_List_Request, Remove_List_Response>(request));
        }

        [HttpPut(Routes.List.Change)]
        public async Task<ActionResult<ResponseBuilder<Change_List_Response>>> Change([FromForm] Change_List_Request request)
        {
            return await Mediator.Send(new Change_Command<Change_List_Request, Change_List_Response>(request));
        }

        [HttpGet(Routes.List.Gets)]
        public async Task<ActionResult<ResponseBuilder<List<Get_Lists_Response>>>> Gets()
        {
            return await Mediator.Send(new Gets_Query<Get_Lists_Request, Get_Lists_Response>());
        }


    }
}
