using Application;
using Infrastructure;
using Microsoft.AspNetCore.Rewrite;
using Todo_Api._;
using Todo_Application;
using Todo_Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
var services = builder.Services;
var configuration = builder.Configuration;


// /// //// /////
services.AddControllers(
    options =>
    {
        //options.ModelValidatorProviders.Clear();
        options.Filters.Add<ExceptionFilter>();
        //options.Filters.Add<ValidationFilter>();
        options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;
    });

services.AddRazorPages()
    .AddViewOptions(options =>
    {
        options.HtmlHelperOptions.ClientValidationEnabled = false;
    });
services.AddSwagger();
services.AddTodoInfrastructure(configuration);
services.AddTodoApplication();

var app = builder.Build();

var option = new RewriteOptions();
option.AddRedirect("^$", "swagger");
app.UseRewriter(option);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");


// /// //// /////
app.UseSwaggerConfigure();

app.Run();
