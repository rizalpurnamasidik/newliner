﻿using Domain.Entities;

namespace Todo_Domain.Todo
{
    public class Todo_Item_Entity : Auditable_Entity<Guid>
    {
        public Guid? ListId { get; set; }
        public string? Task { get; set; } = string.Empty;
        public string? TaskDescription { get; set; } = string.Empty;
        public bool? IsDone { get; private set; } = false;

    }
}
