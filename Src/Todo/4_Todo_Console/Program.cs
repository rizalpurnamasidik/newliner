using Application;
using Infrastructure;
using Serilog;
using Todo_Application;
using Todo_Console;
using Todo_Infrastructure;

var host = Host.CreateDefaultBuilder(args)
    .UseSerilog()
    .ConfigureServices((hostContext, services) =>
    {
        var configurationManager = new ConfigurationManager();
        configurationManager.AddConfiguration(hostContext.Configuration);

        services.AddTodoInfrastructure(configurationManager).AddInfrastructure();
        services.AddTodoApplication().AddApplication();
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
