﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Todo_Shared.Todo_List.Commands.Add_Todo_List
{
    public class Add_Todo_List_Request : BaseRequest
    {
        public string ListName { get; set; } = string.Empty;
    }

}
