﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Todo_Shared.Todo_List.Commands.Change_Todo_List
{
    public class Change_Todo_List_Request : BaseRequest<Guid>
    {
        public string ListName { get; set; } = string.Empty;
    }

}
