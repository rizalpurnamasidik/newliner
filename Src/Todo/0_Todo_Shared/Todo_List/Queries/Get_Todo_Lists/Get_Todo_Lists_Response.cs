﻿using Shared.Responses;

namespace Todo_Shared.Todo_List.Queries.Get_Todo_Lists
{
    public class Get_Todo_Lists_Response : BaseResponse<Guid>
    {
        public string ListName { get; set; } = string.Empty;
    }
}
