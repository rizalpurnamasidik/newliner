﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Todo_Shared.Todo_Item.Queries.Get_Todo_Items
{
    public class Get_Todo_Items_Response : BaseResponse<Guid>
    {
        public Guid ListId { get; set; }
        public string Task { get; set; } = string.Empty;
        public string TaskDescription { get; set; } = string.Empty;
    }
}
