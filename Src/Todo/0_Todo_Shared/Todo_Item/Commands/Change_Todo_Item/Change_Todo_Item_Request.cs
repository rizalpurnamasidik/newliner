﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Todo_Shared.Todo_Item.Commands.Change_Todo_Item
{
    public class Change_Todo_Item_Request : BaseRequest<Guid>
    {
        public Guid ListId { get; set; }
        public string Task { get; set; } = string.Empty;
        public string TaskDescription { get; set; } = string.Empty;
    }

}
