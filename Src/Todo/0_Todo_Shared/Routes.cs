﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo_Shared
{
    public class Routes
    {
        public static class Item
        {
            public const string Gets = "/items";
            public const string Add = "/item";
            public const string Remove = "/item";
            public const string Change = "/item";
        }

        public static class List
        {
            public const string Gets = "/lists";
            public const string Add = "/list";
            public const string Remove = "/list";
            public const string Change = "/list";
        }







    }
}
