﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces.Databases;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Todo_Domain;
using Todo_Domain.Todo;

namespace Todo_Application._.Interfaces
{

    public interface ITodoDbContext : IDatabaseContext
    {
        public DbSet<Todo_Item_Entity> Todo_Item_Entities { get; set; }
        public DbSet<Todo_List_Entity> Todo_List_Entities { get; set; }

    }
}
