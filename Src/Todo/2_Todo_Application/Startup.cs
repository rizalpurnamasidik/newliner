﻿using System.Reflection;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Shared.Responses;
using Todo_Application._.Interfaces;
using Application.GenericApp.Queries.Gets;
using Application;
using Todo_Domain.Todo;
using Todo_Application.Todo.List.Queries.Get_Lists;
using Application.GenericApp.Commands.Add;
using Todo_Application.Todo.List.Commands.Add_List;
using Todo_Application.Todo.List.Commands.Remove_List;
using Application.GenericApp.Commands.Remove;
using Application.GenericApp.Commands.Change;
using Todo_Application.Todo.List.Commands.Change_List;
using Application.Behaviours;

namespace Todo_Application
{
    public static class Startup
    {
        public static IServiceCollection AddTodoApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddApplication();
            services.GenericApp();

            //// Behaviour : Order is important
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(Validation_Behaviour<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthorizationBehaviour<,>));

            return services;

        }

        /// <summary>
        /// untuk proses CRUD yang standar
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection GenericApp(this IServiceCollection services)
        {
            //
            // todo list
            services.AddTransient(typeof(IRequestHandler<Gets_Query<Get_Lists_Request, Get_Lists_Response>, ResponseBuilder<List<Get_Lists_Response>>>)
                , typeof(GetsQueryHandler<ITodoDbContext, Todo_List_Entity, Get_Lists_Request, Get_Lists_Response>));

            services.AddTransient(typeof(IRequestHandler<Add_Command<Add_List_Request, Add_List_Response>, ResponseBuilder<Add_List_Response>>)
                , typeof(AddCommandHandler<ITodoDbContext, Todo_List_Entity, Add_List_Request, Add_List_Response>));

            services.AddTransient(typeof(IRequestHandler<Remove_Command<Remove_List_Request, Remove_List_Response>, ResponseBuilder<Remove_List_Response>>)
                , typeof(RemoveCommandHandler<ITodoDbContext, Todo_List_Entity, Remove_List_Request, Remove_List_Response>));

            services.AddTransient(typeof(IRequestHandler<Change_Command<Change_List_Request, Change_List_Response>, ResponseBuilder<Change_List_Response>>)
                , typeof(ChangeCommandHandler<ITodoDbContext, Todo_List_Entity, Change_List_Request, Change_List_Response>));


            //
            // todo item
            //services.AddTransient(typeof(IRequestHandler<GetsQuery<Get_Todo_Items_Request, Get_Todo_Items_Response>, ResponseBuilder<List<Get_Todo_Items_Response>>>)
            //    , typeof(GetsQueryHandler<ITodoDbContext, Todo_Item_Entity, Get_Todo_Items_Request, Get_Todo_Items_Response>));

            //services.AddTransient(typeof(IRequestHandler<AddCommand<Add_Todo_Item_Request, Add_Todo_Item_Response>, ResponseBuilder<Add_Todo_Item_Response>>)
            //    , typeof(AddCommandHandler<ITodoDbContext, Todo_Item_Entity, Add_Todo_Item_Request, Add_Todo_Item_Response>));

            //services.AddTransient(typeof(IRequestHandler<RemoveCommand<Remove_Todo_Item_Request, Remove_Todo_Item_Response>, ResponseBuilder<Remove_Todo_Item_Response>>)
            //    , typeof(RemoveCommandHandler<ITodoDbContext, Todo_Item_Entity, Remove_Todo_Item_Request, Remove_Todo_Item_Response>));

            //services.AddTransient(typeof(IRequestHandler<ChangeCommand<Change_Todo_Item_Request, Change_Todo_Item_Response>, ResponseBuilder<Change_Todo_Item_Response>>)
            //    , typeof(ChangeCommandHandler<ITodoDbContext, Todo_Item_Entity, Change_Todo_Item_Request, Change_Todo_Item_Response>));



            return services;
        }



    }
}
