﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Extensions;
using Dapper;
using Domain;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Responses;
using Todo_Application._.Interfaces;
using Todo_Shared.Todo.Queries.GetTodos;

namespace Todo_Application.Todo.Queries.GetTodos
{
    public class GetTodosQuery : GetTodosRequest, IRequest<ResponseBuilder<List<GetTodosResponse>>>
    //public class GetTodosQuery<TModel> : IRequest<ResponseBuilder<List<TModel>>> where TModel : Entity<Guid>
    {
    }


    //public class Handler<TEntity, TModel> : IRequestHandler<GetTodosQuery<TModel>, ResponseBuilder<List<TModel>>>
    public class Handler<TEntity> : IRequestHandler<GetTodosQuery, ResponseBuilder<List<GetTodosResponse>>> //where TEntity : class
    {
        private readonly ILogger _logger;
        private readonly IDbWrapper _dbWrapper;

        public Handler(ILogger logger,
            IDbWrapper dbWrapper
            )
        {
            _logger = logger;
            _dbWrapper = dbWrapper;
        }

        public async Task<ResponseBuilder<List<GetTodosResponse>>> Handle(GetTodosQuery request, CancellationToken cancellationToken)
        {
            //var aaaa = _dbWrapper.TodoDbContext.Instance.Set<request.E>().ToList();

            // get data
            var data = _dbWrapper.TodoDbContext.Public_Todos.
                Select(s => new GetTodosResponse
                {
                    Id = s.Id,
                    Todo = s.Todo
                }).
                ToList();

            // return
            return data.ResponseRead();
        }
    }
}
