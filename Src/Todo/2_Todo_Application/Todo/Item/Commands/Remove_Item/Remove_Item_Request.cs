﻿using Shared.Requests;

namespace Todo_Application.Todo.Item.Commands.Remove_Item
{
    public class Remove_Item_Request : Base_Request
    {
        public Guid? Id { get; set; }
    }
}
