﻿using Application.Extensions;
using MediatR;
using Shared.Exceptions;
using Shared.Responses;
using Todo_Application._.Interfaces;

namespace Todo_Application.Todo.Item.Commands.Remove_Item
{
    public class Remove_Item_Command : Remove_Item_Request, IRequest<ResponseBuilder<Remove_Item_Response>>
    {

    }

    public class Handler : IRequestHandler<Remove_Item_Command, ResponseBuilder<Remove_Item_Response>>
    {
        private readonly ITodoDbContext _todoDb;

        public Handler(
            ITodoDbContext todoDb
            )
        {
            _todoDb = todoDb;
        }

        public async Task<ResponseBuilder<Remove_Item_Response>> Handle(Remove_Item_Command request, CancellationToken cancellationToken)
        {
            // find data
            var dataToRemove = _todoDb.Todo_Item_Entities.FirstOrDefault(x => x.Id == request.Id);

            // validation
            if (dataToRemove == null)
            {
                throw new NotFound_Exception();
            }

            // remove data
            _todoDb.Todo_Item_Entities.Remove(dataToRemove);
            await _todoDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Remove_Item_Response
            {
                Id = dataToRemove.Id
            }.ResponseRemove();
        }


    }
}
