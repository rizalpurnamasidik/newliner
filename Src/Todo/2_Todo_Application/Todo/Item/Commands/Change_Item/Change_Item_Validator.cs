﻿using FluentValidation;

namespace Todo_Application.Todo.Item.Commands.Change_Item
{
    public class Add_Item_Validator : AbstractValidator<Change_Item_Command>
    {
        public Add_Item_Validator()
        {
            RuleFor(r => r.Id).NotEmpty();
            RuleFor(r => r.ListId).NotEmpty();
            RuleFor(r => r.TaskDescription).NotEmpty();
            RuleFor(r => r.Task).NotEmpty();
        }
    }
}
