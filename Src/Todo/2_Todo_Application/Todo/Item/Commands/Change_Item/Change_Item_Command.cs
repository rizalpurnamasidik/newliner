﻿using Application.Extensions;
using MediatR;
using Shared.Exceptions;
using Shared.Extensions;
using Shared.Responses;
using Todo_Application._.Interfaces;
using Todo_Domain.Todo;

namespace Todo_Application.Todo.Item.Commands.Change_Item
{
    public class Change_Item_Command : Change_Item_Request, IRequest<ResponseBuilder<Change_Item_Response>>
    {

    }

    public class Handler : IRequestHandler<Change_Item_Command, ResponseBuilder<Change_Item_Response>>
    {
        private readonly ITodoDbContext _todoDb;

        public Handler(
           ITodoDbContext todoDb
            )
        {
            _todoDb = todoDb;
        }

        public async Task<ResponseBuilder<Change_Item_Response>> Handle(Change_Item_Command request, CancellationToken cancellationToken)
        {
            // find data
            var dataToChange = _todoDb.Todo_Item_Entities.FirstOrDefault(x => x.Id == request.Id);

            // validation
            if (dataToChange == null)
            {
                throw new NotFound_Exception();
            }

            // change data
            request.CopyAllTo(dataToChange);

            _todoDb.Todo_Item_Entities.Update(dataToChange);
            await _todoDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Change_Item_Response
            {
                Id = dataToChange.Id
            }.ResponseChange();
        }


    }
}
