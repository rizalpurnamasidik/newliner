﻿using FluentValidation;

namespace Todo_Application.Todo.Item.Commands.Add_Items
{
    public class Add_Item_Validator : AbstractValidator<Add_Item_Command>
    {
        public Add_Item_Validator()
        {
            RuleFor(r => r.ListId).NotEmpty();
            RuleFor(r => r.TaskDescription).NotEmpty();
            RuleFor(r => r.Task).NotEmpty();
        }
    }
}
