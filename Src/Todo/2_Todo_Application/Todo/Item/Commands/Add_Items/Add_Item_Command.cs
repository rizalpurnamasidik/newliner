﻿using Application.Extensions;
using MediatR;
using Shared.Responses;
using Todo_Application._.Interfaces;
using Todo_Domain.Todo;

namespace Todo_Application.Todo.Item.Commands.Add_Items
{
    public class Add_Item_Command : Add_Item_Request, IRequest<ResponseBuilder<Add_Item_Response>>
    {

    }

    public class Handler : IRequestHandler<Add_Item_Command, ResponseBuilder<Add_Item_Response>>
    {
        private readonly ITodoDbContext _todoDb;

        public Handler(
           ITodoDbContext todoDb
            )
        {
            _todoDb = todoDb;
        }

        public async Task<ResponseBuilder<Add_Item_Response>> Handle(Add_Item_Command request, CancellationToken cancellationToken)
        {
            // build data to save
            var dataToSave = new Todo_Item_Entity
            {
                ListId = request.ListId,
                TaskDescription = request.TaskDescription,
                Task = request.Task,
            };

            // save data
            _todoDb.Todo_Item_Entities.Add(dataToSave);
            await _todoDb.SaveChangesAsync(cancellationToken);


            // return 
            return new Add_Item_Response
            {
                Id = dataToSave.Id
            }.ResponseCreate();
        }


    }
}
