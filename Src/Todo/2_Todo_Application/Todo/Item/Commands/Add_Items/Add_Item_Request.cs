﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Todo_Application.Todo.Item.Commands.Add_Items
{
    public class Add_Item_Request : Base_Request
    {
        public Guid? ListId { get; set; }
        public string? Task { get; set; } = string.Empty;
        public string? TaskDescription { get; set; } = string.Empty;
    }

}
