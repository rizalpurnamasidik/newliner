﻿using Application.Extensions;
using Application.Interfaces.Logs;
using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Responses;
using Todo_Application._.Interfaces;

namespace Todo_Application.Todo.Item.Queries.Get_Items
{
    public class Get_Items_Query : Get_Items_Request, IRequest<ResponseBuilder<List<Get_Items_Response>>>
    {

    }

    public class Handler : IRequestHandler<Get_Items_Query, ResponseBuilder<List<Get_Items_Response>>>
    {
        private readonly ITodoDbContext _todoDb;
        private readonly ILog _log;

        public Handler(
            ITodoDbContext todoDb
            , ILog log
            )
        {
            _todoDb = todoDb;
            _log = log;
        }

        public async Task<ResponseBuilder<List<Get_Items_Response>>> Handle(Get_Items_Query request, CancellationToken cancellationToken)
        {


            // get data with ef core
            //var data = await _todoDb.Todo_Item_Entities
            //    .Select(s => new Get_Items_Response
            //    {
            //        Id = s.Id,
            //        ListId = s.ListId,
            //        Task = s.Task,
            //        TaskDescription = s.TaskDescription

            //    })
            //    .ToListAsync();

            // get data with dapper
            _log.LogInformation("ini menggunakan dapper");
            var query = "SELECT * FROM Todo.item";
            var data = _todoDb.Dapper().Query<Get_Items_Response>(query).ToList();


            // return 
            return data.ResponseRead();
        }


    }
}
