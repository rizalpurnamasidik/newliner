﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Todo_Application.Todo.Item.Queries.Get_Items
{
    public class Get_Items_Response : Base_Response<Guid>
    {
        public Guid? ListId { get; set; }
        public string? Task { get; set; } = string.Empty;
        public string? TaskDescription { get; set; } = string.Empty;
    }
}
