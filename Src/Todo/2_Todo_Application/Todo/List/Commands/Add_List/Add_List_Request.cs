﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Todo_Application.Todo.List.Commands.Add_List
{
    public class Add_List_Request : Base_Request
    {
        public string ListName { get; set; } = string.Empty;
    }

}
