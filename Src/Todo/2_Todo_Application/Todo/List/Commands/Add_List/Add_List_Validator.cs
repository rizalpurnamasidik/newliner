﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Todo_Application.Todo.List.Commands.Add_List
{
    public class Add_List_Validator : AbstractValidator<Add_List_Request>
    {
        public Add_List_Validator()
        {
            RuleFor(r => r.ListName).NotEmpty();
        }
    }
}
