﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Requests;

namespace Todo_Application.Todo.List.Commands.Change_List
{
    public class Change_List_Request : Base_Request<Guid>
    {
        public string ListName { get; set; } = string.Empty;
    }

}
