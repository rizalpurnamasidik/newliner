﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Responses;

namespace Todo_Application.Todo.List.Queries.Get_Lists
{
    public class Get_Lists_Response : Base_Response<Guid>
    {
        public string ListName { get; set; } = string.Empty;
    }
}
