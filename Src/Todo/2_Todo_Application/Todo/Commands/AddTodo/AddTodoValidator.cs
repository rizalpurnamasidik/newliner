﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Todo_Application.Todo.Commands.AddTodo
{
    public class AddTodoValidator : AbstractValidator<AddTodoCommand>
    {
        public AddTodoValidator()
        {
            RuleFor(r => r.Todo).MaximumLength(100).NotNull();
        }
    }
}
