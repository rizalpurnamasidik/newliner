﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Extensions;
using Dapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Exceptions;
using Shared.Responses;
using Todo_Application._.Interfaces;
using Todo_Domain;
using Todo_Shared.Todo.Commands.AddTodo;

namespace Todo_Application.Todo.Commands.AddTodo
{

    public class AddTodoCommand : AddTodoRequest, IRequest<ResponseBuilder<AddTodoResponse>>
    {

    }

    public class Handler : IRequestHandler<AddTodoCommand, ResponseBuilder<AddTodoResponse>>
    {
        private readonly ILogger<Handler> _logger;
        private readonly IDbWrapper _dbWrapper;

        public Handler(
            ILogger<Handler> logger,
            IDbWrapper dbWrapper
            )
        {
            _logger = logger;
            _dbWrapper = dbWrapper;
        }

        public async Task<ResponseBuilder<AddTodoResponse>> Handle(AddTodoCommand request, CancellationToken cancellationToken)
        {

            //var aaa = _dbWrapper.TodoDbContext.Dapper().Query<Public_TodoEntity>("SELECT * FROM TODO").ToList();

            //using (var conn = _dbWrapper.TodoDbContext.Dapper())
            //{
            //    conn.Open();
            //    using var transaction = conn.BeginTransaction();
            //    var aa = 1;
            //    transaction.Commit();
            //}

            // add new todo
            var newData = new Public_TodoEntity
            {
                Todo = request.Todo,
            };

            // save
            _dbWrapper.TodoDbContext.Public_Todos.Add(newData);
            _dbWrapper.TodoDbContext.SaveChanges();

            // return
            return new AddTodoResponse
            {
                Id = newData.Id
            }.ResponseRead();
        }


    }
}
