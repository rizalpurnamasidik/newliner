﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Todo_Domain.Todo;

namespace Todo_Infrastructure.DataSources.Todo.Configs
{

    public class Todo_Item_Config : IEntityTypeConfiguration<Todo_Item_Entity>
    {
        public void Configure(EntityTypeBuilder<Todo_Item_Entity> builder)
        {
            builder.ToTable("item", "Todo");
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(p => p.Task).HasMaxLength(200);
            builder.Property(p => p.TaskDescription).HasMaxLength(1000);
        }
    }
}
