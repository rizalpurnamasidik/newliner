﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Todo_Domain.Todo;

namespace Todo_Infrastructure.DataSources.Todo.Configs
{

    public class Todo_List_Config : IEntityTypeConfiguration<Todo_List_Entity>
    {
        public void Configure(EntityTypeBuilder<Todo_List_Entity> builder)
        {
            builder.ToTable("List", "Todo");
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(p => p.ListName).HasMaxLength(200);
        }
    }
}
