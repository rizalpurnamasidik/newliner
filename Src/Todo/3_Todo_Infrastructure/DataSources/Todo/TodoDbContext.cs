﻿using Application.Interfaces;
using Infrastructure.DataSources;
using Infrastructure.Extension;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Todo_Application._.Interfaces;
using Todo_Domain.Todo;
using Todo_Infrastructure.Settings;

namespace Todo_Infrastructure.DataSources.Todo
{

    public class TodoDbContext : DatabaseContext, ITodoDbContext
    {

        public DbContext Instance => this;

        public TodoDbContext(DbContextOptions<TodoDbContext> options,
            IOptions<TodoSettings> config
            , IUserLogin userLogin
            ) : base(options, config, userLogin)
        {
            DapperInit(nameof(TodoDbContext));
        }

        public DbSet<Todo_Item_Entity> Todo_Item_Entities { get; set; }
        public DbSet<Todo_List_Entity> Todo_List_Entities { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyAllConfigurationsFromNameSpace("Todo_Infrastructure.Databases.Todo.Configs");
        }
    }
}
