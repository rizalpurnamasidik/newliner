﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Todo_Application._.Interfaces;
using Todo_Infrastructure.Settings;
using Infrastructure;
using Infrastructure.Extension;
using Todo_Infrastructure.DataSources.Todo;

namespace Todo_Infrastructure
{
    public static class Startup
    {
        public static IServiceCollection AddTodoInfrastructure(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddInfrastructure();
            services.LoadSettings<TodoSettings>(configuration);

            services.AddDbContext(configuration, db =>
            {
                db.DbConnect<ITodoDbContext, TodoDbContext>(configuration);
            });


            return services;
        }
    }
}
